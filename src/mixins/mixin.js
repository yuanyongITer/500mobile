export default {
	methods: {
		formateLowercase(value) {
		  if (!value) {
		  	return;
		  } else {
		  	return (value.toString()).replace(/\ /g, "").toLowerCase();
		  }
		}
  }
}