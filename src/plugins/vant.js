import Vue from 'vue'
import 'vant/lib/index.css';
import Form from 'vant/lib/form';
import Field from 'vant/lib/field';
import Button from 'vant/lib/button';
import Radio from 'vant/lib/radio';
import RadioGroup from 'vant/lib/radio-group';
import Icon from 'vant/lib/icon';
import Stepper from 'vant/lib/stepper';
import Picker from 'vant/lib/picker';
import Popup from 'vant/lib/popup';
import Cell from 'vant/lib/cell';
import CellGroup from 'vant/lib/cell-group';
import Checkbox from 'vant/lib/checkbox';
import CheckboxGroup from 'vant/lib/checkbox-group';
import Switch from 'vant/lib/switch';
import Collapse from 'vant/lib/collapse';
import CollapseItem from 'vant/lib/collapse-item';
import Loading from 'vant/lib/loading';

Vue.component('van-form', Form);
Vue.component('van-field', Field);
Vue.component('van-icon', Icon);
Vue.component('van-button', Button);
Vue.component('van-radio', Radio);
Vue.component('van-radio-group', RadioGroup);
Vue.component('van-stepper', Stepper);
Vue.component('van-picker', Picker);
Vue.component('van-popup', Popup);
Vue.component('van-cell', Cell);
Vue.component('van-group', CellGroup);
Vue.component('van-checkbox', Checkbox);
Vue.component('van-checkbox-group', CheckboxGroup);
Vue.component('van-switch', Switch);
Vue.component('van-collapse', Collapse);
Vue.component('van-collapse-item', CollapseItem);
Vue.component('van-loading', Loading);