import Vue from 'vue'
import App from './App.vue'
import Vuex from "vuex"
import axios from 'axios'
import vueAxios from 'vue-axios'
import router from './router/index'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueLazyLoad from 'vue-lazyload'
import store from './store'
import '@/plugins/vant'
import config from '@/config/index'
import '@/config/rem'

import componentsList from '@/components/componentsRegister';
Vue.use(componentsList);

import { Toast } from 'vant';

import 'swiper/dist/css/swiper.css'
import '@/assets/css/fonts/fonts.css'
import '@/assets/css/base/base.css'
import '@/assets/scss/public/_public.scss'

import mock from './mock'

const isMock = true;
if(isMock) {
  mock.init()
}

if (process.env.NODE_ENV !== 'production') {
  Vue.config.performance = true;
}

Vue.use(vueAxios,axios)

Vue.use(Vuex)

Vue.use(VueAwesomeSwiper)

Vue.use(VueLazyLoad,{
  error:'/images/public/logo.png',
  loading:'/images/public/logo.png'
})

Vue.prototype.$config = config;

Vue.use(Toast);

Vue.config.productionTip = false

new Vue({
	store,
	router,
  render: h => h(App),
}).$mount('#app')
