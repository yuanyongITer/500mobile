
export const forEach = (arr, fn) => {
  if (!arr.length || !fn) return
  let i = -1
  let len = arr.length
  while (++i < len) {
    let item = arr[i]
    fn(item, i, arr)
  }
}

/**
 * @param {Array} arr1
 * @param {Array} arr2
 * @description 得到两个数组的交集, 两个数组的元素为数值或字符串
 */
export const getIntersection = (arr1, arr2) => {
  let len = Math.min(arr1.length, arr2.length)
  let i = -1
  let res = []
  while (++i < len) {
    const item = arr2[i]
    if (arr1.indexOf(item) > -1) res.push(item)
  }
  return res
}

/**
 * @param {Array} arr1
 * @param {Array} arr2
 * @description 得到两个数组的并集, 两个数组的元素为数值或字符串
 */
export const getUnion = (arr1, arr2) => {
  return Array.from(new Set([...arr1, ...arr2]))
}

/**
 * @param {Array} target 目标数组
 * @param {Array} arr 需要查询的数组
 * @description 判断要查询的数组是否至少有一个元素包含在目标数组中
 */
export const hasOneOf = (targetarr, arr) => {
  return targetarr.some(_ => arr.indexOf(_) > -1)
}

/**
 * @param {String|Number} value 要验证的字符串或数值
 * @param {*} validList 用来验证的列表
 */
export function oneOf (value, validList) {
  for (let i = 0; i < validList.length; i++) {
    if (value === validList[i]) {
      return true
    }
  }
  return false
}

/**
 * @param {Number} timeStamp 判断时间戳格式是否是毫秒
 * @returns {Boolean}
 */
const isMillisecond = timeStamp => {
  const timeStr = String(timeStamp)
  return timeStr.length > 10
}

/**
 * @param {Number} timeStamp 传入的时间戳
 * @param {Number} currentTime 当前时间时间戳
 * @returns {Boolean} 传入的时间戳是否早于当前时间戳
 */
const isEarly = (timeStamp, currentTime) => {
  return timeStamp < currentTime
}

/**
 * @param {Number} num 数值
 * @returns {String} 处理后的字符串
 * @description 如果传入的数值小于10，即位数只有1位，则在前面补充0
 */
const getHandledValue = num => {
  return num < 10 ? '0' + num : num
}

/**
 * @param {Number} timeStamp 传入的时间戳
 * @param {Number} startType 要返回的时间字符串的格式类型，传入'year'则返回年开头的完整时间
 */
const getDate = (timeStamp, startType) => {
  const d = new Date(timeStamp * 1000)
  const year = d.getFullYear()
  const month = getHandledValue(d.getMonth() + 1)
  const date = getHandledValue(d.getDate())
  const hours = getHandledValue(d.getHours())
  const minutes = getHandledValue(d.getMinutes())
  const second = getHandledValue(d.getSeconds())
  let resStr = ''
  if (startType === 'year') resStr = year + '-' + month + '-' + date + ' ' + hours + ':' + minutes + ':' + second
  else resStr = month + '-' + date + ' ' + hours + ':' + minutes
  return resStr
}

/**
 * @param {String|Number} timeStamp 时间戳
 * @returns {String} 相对时间字符串
 */
export const getRelativeTime = timeStamp => {
  // 判断当前传入的时间戳是秒格式还是毫秒
  const IS_MILLISECOND = isMillisecond(timeStamp)
  // 如果是毫秒格式则转为秒格式
  if (IS_MILLISECOND) Math.floor(timeStamp /= 1000)
  // 传入的时间戳可以是数值或字符串类型，这里统一转为数值类型
  timeStamp = Number(timeStamp)
  // 获取当前时间时间戳
  const currentTime = Math.floor(Date.parse(new Date()) / 1000)
  // 判断传入时间戳是否早于当前时间戳
  const IS_EARLY = isEarly(timeStamp, currentTime)
  // 获取两个时间戳差值
  let diff = currentTime - timeStamp
  // 如果IS_EARLY为false则差值取反
  if (!IS_EARLY) diff = -diff
  let resStr = ''
  const dirStr = IS_EARLY ? '前' : '后'
  // 少于等于59秒
  if (diff <= 59) resStr = diff + '秒' + dirStr
  // 多于59秒，少于等于59分钟59秒
  else if (diff > 59 && diff <= 3599) resStr = Math.floor(diff / 60) + '分钟' + dirStr
  // 多于59分钟59秒，少于等于23小时59分钟59秒
  else if (diff > 3599 && diff <= 86399) resStr = Math.floor(diff / 3600) + '小时' + dirStr
  // 多于23小时59分钟59秒，少于等于29天59分钟59秒
  else if (diff > 86399 && diff <= 2623859) resStr = Math.floor(diff / 86400) + '天' + dirStr
  // 多于29天59分钟59秒，少于364天23小时59分钟59秒，且传入的时间戳早于当前
  else if (diff > 2623859 && diff <= 31567859 && IS_EARLY) resStr = getDate(timeStamp)
  else resStr = getDate(timeStamp, 'year')
  return resStr
}

/**
 * @returns {String} 当前浏览器名称
 */
export const getExplorer = () => {
  const ua = window.navigator.userAgent
  const isExplorer = (exp) => {
    return ua.indexOf(exp) > -1
  }
  if (isExplorer('MSIE')) return 'IE'
  else if (isExplorer('Firefox')) return 'Firefox'
  else if (isExplorer('Chrome')) return 'Chrome'
  else if (isExplorer('Opera')) return 'Opera'
  else if (isExplorer('Safari')) return 'Safari'
}

/**
 * @description 绑定事件 on(element, event, handler)
 */
export const on = (function () {
  if (document.addEventListener) {
    return function (element, event, handler) {
      if (element && event && handler) {
        element.addEventListener(event, handler, false)
      }
    }
  } else {
    return function (element, event, handler) {
      if (element && event && handler) {
        element.attachEvent('on' + event, handler)
      }
    }
  }
})()

/**
 * @description 解绑事件 off(element, event, handler)
 */
export const off = (function () {
  if (document.removeEventListener) {
    return function (element, event, handler) {
      if (element && event) {
        element.removeEventListener(event, handler, false)
      }
    }
  } else {
    return function (element, event, handler) {
      if (element && event) {
        element.detachEvent('on' + event, handler)
      }
    }
  }
})()

/**
 * 判断一个对象是否存在key，如果传入第二个参数key，则是判断这个obj对象是否存在key这个属性
 * 如果没有传入key这个参数，则判断obj对象是否有键值对
 */
export const hasKey = (obj, key) => {
  if (key) return key in obj
  else {
    let keysArr = Object.keys(obj)
    return keysArr.length
  }
}

/**
 * @param {*} obj1 对象
 * @param {*} obj2 对象
 * @description 判断两个对象是否相等，这两个对象的值只能是数字或字符串
 */
export const objEqual = (obj1, obj2) => {
  const keysArr1 = Object.keys(obj1)
  const keysArr2 = Object.keys(obj2)
  if (keysArr1.length !== keysArr2.length) return false
  else if (keysArr1.length === 0 && keysArr2.length === 0) return true
  else return !keysArr1.some(key => obj1[key] != obj2[key])
}

/**
 * @param {String} url
 * @description 从URL中解析参数
 */
export const getParams = url => {
  const keyValueArr = url.split('?')[1].split('&')
  let paramObj = {}
  keyValueArr.forEach(item => {
    const keyValue = item.split('=')
    paramObj[keyValue[0]] = keyValue[1]
  })
  return paramObj
}

/**
 * @param {num} 随机个数
 * @param {size} 总的数字个数
 * @description 返回一个从size中产生num个随机号码组成的数组
 */
export const produceRndArr = (num,size) => {
  let arr = [];
  let tmp = [];
  for (let i=1; i<=num; i++) {
    arr.push(i);
  }
  for (let j=0; j<size; j++) {
    let n = Math.floor(Math.random() * arr.length);
    tmp.push(arr[n]);
    arr.splice(n, 1);
  }
  tmp.sort((a,b)=>{return a-b});
  return tmp;
}

/**
 * @description 往localStorage设置数据
 */
export const localSave = (key, value) => {
  localStorage.setItem(key, value);
}

/**
 * @description 读取localStorage存储的数据
 */
export const localRead = (key) => {
  return localStorage.getItem(key) || '';
}

/**
 * @description 删除localStorage存储的数据
 */
export const localRemove = (key) => {
  localStorage.removeItem(key);
}

/**
 * @description Modal显示的时候固定页面窗口
 */
export const freezeAppModal = () => {
  if ( !document.documentElement.classList.contains('fixed') ) {
    document.documentElement.classList.add('fixed');
  }
}

/**
 * @description Modal隐藏的时候恢复页面行为
 */
export const thawAppModal = () => {
  if ( document.documentElement.classList.contains('fixed') ) {
    document.documentElement.classList.remove('fixed');
  }
}

export const fixedAppView = (isModalFixed) => {
  let top = window.scrollY, root = document.documentElement;
  if (isModalFixed) {
    window.scrollTo(0, top);
    localSave('scrollTop',top);
    freezeAppModal();
  } else {
    thawAppModal();
    window.scrollTo(0, Number(localRead('scrollTop')));
    localRemove('scrollTop');
  }
}

/**
 * 格式化金额
 * @param {[type]} s  [要转换的数字]
 * @param {[type]} n  [小数点位数,默认2位]
 * @return [返回转换完的字符串]
 */
export function formatMoney (s, n) {
  n = n > 0 && n <= 20 ? n : 2;
  s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
  let l = s.split(".")[0].split("").reverse();
  let r = s.split(".")[1];
  let t = "";
  for (let i = 0; i < l.length; i++) {
    t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
  }
  return t.split("").reverse().join("") + "." + r;
}

/**
 * @description 返回当前时间的一个键值对格式的日期对象
 * @return {object} {'Y':2020,'M':12,'D':18,'h':10,'m':24,'s':18}
 */
export const getNowDate = () => {
  let date = new Date();
  return {
    'Y':date.getFullYear(),
    'M':date.getMonth()+1,
    'D':date.getDate(),
    'h':date.getHours(),
    'm':date.getMinutes(),
    's':date.getSeconds()
  }
}

/**
 * @description 返回一个键值对格式的日期对象
 * @param {string} drawDate => '2020-12-18 20:57:00'
 * @return {object} {'Y':2020,'M':12,'D':18,'h':10,'m':24,'s':18}
 */
export const getDrawDate = (drawDate) => {
  let dateTimeArr = drawDate.split(" ");
  let dateArr = (dateTimeArr[0]).split("-");
  let timeArr = (dateTimeArr[1]).split(":");
  return {
    "Y":dateArr[0]||0,
    "M":dateArr[1]||0,
    "D":dateArr[2]||1,
    "h":timeArr[0]||0,
    "m":timeArr[1]||0,
    "s":timeArr[2]||0
  }
}

/**
 * @description 返回一个时间戳
 * @param {string} drawDate => {'Y':2020,'M':12,'D':18,'h':10,'m':24,'s':18}
 * @param {string} nowDate => {'Y':2020,'M':12,'D':28,'h':10,'m':24,'s':18}
 * @return {number} 40962000
 */
export const getTimeStamp = (drawdateTime) => {
  let nowTimeStamp = new Date().getTime();
  let endTimeStamp = new Date(drawdateTime).getTime(); 
  return (endTimeStamp - nowTimeStamp);
}

/**
 * @description 返回一个字符串
 * @param {string} drawdateTime => '2020-12-18 20:57:00'
 * @return {string} '01 day 18 hours 10 minutes 40 s'
 */
export const getDrawDateTime = (drawdateTime) => {
  let nowTimeStamp = new Date().getTime();
  let endTimeStamp = new Date(drawdateTime).getTime();
  let mss = endTimeStamp - nowTimeStamp;
  let days = parseInt(mss / (1000 * 60 * 60 * 24));
  let hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = parseInt((mss % (1000 * 60)) / 1000);
  if (endTimeStamp < nowTimeStamp) return;
  return days+' days '+hours+' hours '+minutes+' minutes '+seconds+' s'
}

/**
 * @description 返回一个字符串
 * @param {string} drawDate => '2020-12-18 20:57:00'
 * @param {string} nowDate => '2020-12-16 20:57:00'
 * @return {string} 'play/calculat/wait'
 */
export const getDrawstate = (drawdateTime) => {
  let timeStamp = getTimeStamp(drawdateTime);
  if (timeStamp>(1000*60*60*1)) {
    return 'play';
  }
  if (0<timeStamp && timeStamp<=(1000*60*60*1)) {
    return 'calculat';
  }
  if (timeStamp <=0) {
    return 'wait';
  }
}

/**
 * @description 返回一个布尔值
 * @param {string} drawDate => '2020-12-18 20:57:00'
 * @param {string} nowDate => '2020-12-16 20:57:00'
 * @return {boolean} true/false
 */
export const isPlayAble = (drawDate,nowDate) => {
  let timeStamp = getTimeStamp(drawDate,nowDate);
  if (timeStamp>(1000*60*60*2)) {
    return true;
  } else {
    return false;
  }
}

/**
 * @description 返回一个布尔值
 * @param {string} drawDate => '2020-12-18 20:57:00'
 * @param {string} nowDate => '2020-12-16 20:57:00'
 * @return {boolean} true/false
 */
export const isCalculatJackpot = (drawDate,nowDate) => {
  let timeStamp = getTimeStamp(drawDate,nowDate);
  if (0<timeStamp && timeStamp<=(1000*60*60*2)) {
    return true;
  } else {
    return false;
  }
}

/**
 * @description 返回一个布尔值
 * @param {string} drawDate => '2020-12-18 20:57:00'
 * @param {string} nowDate => '2020-12-16 20:57:00'
 * @return {boolean} true/false
 */
export const isWaitDrawResult = (drawDate,nowDate) => {
  let timeStamp = this.getTimeStamp(drawDate,nowDate);
  if (timeStamp <=0) {
    return true;
  } else {
    return false;
  }
}
