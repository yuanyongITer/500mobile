
import { objEqual } from '@/lib/tools'

export const initAgeLimitedTags = ()=> {
  (localRead('over18')=='') && localSave('over18',true);
}

export const initNewUserTags = () => {
  (localRead('isNewUser')=='') && localSave('isNewUser',1);
}

export const initSubscribeTags = () => {
  (localRead('isSubscribe')=='') && localSave('isSubscribe',0);
}

export const initTokenTags = () => {
  (localRead('token')=='') && localSave('token','');
}

export const authorize = (to, next) => {
  if (localRead('token')) {
    if (['login','register'].includes(to.name)) next({name:'index'})
    else next()
  } else {
    if (to.meta.requireAuth) {
      if (localRead('isNewUser')===1) next({name: 'register',query: {path: to.fullPath}})
      else next({name: 'login',query: {path: to.fullPath}})
    } else {
      next();
    }
  }
}

/**
 * @description 返回一个校验规则对象
 * @return {object}
 */
export const rules = () => {
  let validateProtocol = (value, rule) => {
    return (value==true)?true:false; 
  };
  return {
    userName: [
      { required:true, message:'UserName cannot be empty', trigger:'onBlur' },
      { pattern: /^[a-zA-Z0-9_]{4,15}$/, message:'Incorrect account format', trigger:'onBlur' }
    ],
    password: [
      { required:true, message:'Password cannot be empty', trigger:'onBlur' },
      { pattern: /^[a-zA-Z0-9_]{6,18}$/, message:'Incorrect Password format', trigger:'onBlur' }
    ],
    phone: [
      { required:true, message:'Phone cannot be empty', trigger:'onBlur' },
      { pattern: /^(13[0-9]|15[0-9]|17[0-9]|18[0-9]|19[8|9])\d{8}$/, message:'Incorrect Phone format', trigger:'onBlur' }
    ],
    email: [
      { required:true, message:'Email cannot be empty', trigger:'onBlur' },
      { pattern: /^([a-zA-Z]|[0-9])(\w|\_)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/, message:'Incorrect Email format', trigger:'onBlur' }
    ],
    name: [
      { required: true, message: 'name cannot be empty', trigger:'onChange' }
    ],
    // gender: [
    //   { required:true, message:'Please select gender', trigger:'onChange' }
    // ],
    protocol: [
      { validator: validateProtocol, message:'Please select protocol', trigger:'onChange' }
    ]
  }
}

/**
 * @description 根据name/params/query判断两个路由对象是否相等
 * @param {*} route1 路由对象
 * @param {*} route2 路由对象
 */
export const routeEqual = (route1, route2) => {
  const params1 = route1.params || {}
  const params2 = route2.params || {}
  const query1 = route1.query || {}
  const query2 = route2.query || {}
  return (route1.name === route2.name) && objEqual(params1, params2) && objEqual(query1, query2)
}

export const getRouteTitleHandled = (route) => {
  let router = { ...route }
  let meta = { ...route.meta}
  let title = ''
  if (meta.title) {
    if (typeof meta.title === 'function') {
      meta.__titleIsFunction__ = true
      title = meta.title(route)
    } else {
      title = meta.title
    }
  }
  meta.title = title
  router.meta = meta
  return router
}

export const showTitle = (item) => {
  let { title, __titleIsFunction__ } = item.meta
  if (!title) {
    return;
  } else {
    return title;
  }
}

/**
 * @description 根据当前跳转的路由设置显示在浏览器标签的title
 * @param {Object} routeItem 路由对象
 * @param {Object} vm Vue实例
 */
 export const setTitle = (routeItem) => {
  const handledRoute = getRouteTitleHandled(routeItem)
  const pageTitle = showTitle(handledRoute)
  window.document.title = pageTitle
 }

export const localSave = (key, value) => {
  localStorage.setItem(key, value)
}

export const localRead = (key) => {
  return localStorage.getItem(key) || ''
}