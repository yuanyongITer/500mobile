import Vue from 'vue'
import Router from 'vue-router'
import routers from './routers'
import store from '../store'
import { setTitle, initAgeLimitedTags, initNewUserTags, initSubscribeTags, initTokenTags, authorize } from '@/lib/utils'

Vue.use(Router)

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
}

const router = new Router({
	mode: 'history',
	routes: routers
})

router.beforeEach((to, from, next) => {

	store.commit('setAppLoading', true);

	initAgeLimitedTags();

	initTokenTags();

	initNewUserTags();

	initSubscribeTags();

	authorize(to, next);
})

router.afterEach((to,from)=>{
	store.commit('setAppLoading', false);
	setTitle(to,router.app);
	window.scrollTo(0,0);
})

export default router