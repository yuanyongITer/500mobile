import Main from '@/components/main/layout.vue'

export default [
  {
		path:'/',
		name:'index',
		component: Main,
		redirect:'index',
    children:[
	    {
	    	path:'index',
	    	name:'index',
	    	meta:{
	    		title: 'Online Lottery - Play Lotto Online at Multilotto'
	    	},
	    	component:() => import('@/view/index/index.vue')
	    }
    ]
	},
	{
		path:'/ticket',
		name:'ticket',
		component: Main,
    children:[
	    {
	    	path:'ticket/:id',
	    	name:'ticket',
	    	meta:{
	    		title: 'Play EuroJackpot - Buy Eurojackpot Tickets Online'
	    	},
	    	component:() => import('@/view/ticket/index.vue')
	    }
    ]
	},
	{
		path:'/register',
		name:'register',
		meta:{
      title: 'Sign Up'
    },
		component:() => import('@/view/register/index.vue')
	},
	{
		path:'/login',
		name:'login',
		meta:{
      title: 'Login'
    },
		component:() => import('@/view/login/index.vue')
	},
	{
		path:'/checkout',
		name:'checkout',
		component: Main,
		children:[
			{
				path:'checkout',
				name:'checkout',
				meta:{
		      title: 'Online Lottery - Play Lotto Online at Multilotto',
		      requireAuth: true
		    },
				component:() => import(/* webpackChunkName: 'checkout' */ '@/view/checkout/index.vue')
			},
			{
				path:'complete',
				name:'complete',
				meta:{
		      title: 'Online Lottery - Play Lotto Online at Multilotto',
		      requireAuth: true
		    },
				component:() => import(/* webpackChunkName: 'checkout' */ '@/view/checkout/complete.vue')
			}
		]
	},
	{
		path:'/deposit',
		name:'deposit',
		component: Main,
		children:[
			{
				path:'deposit',
		    name:'deposit',
				meta:{
		      title: 'Online Lottery - Play Lotto Online at Multilotto',
		      requireAuth: true
		    },
				component:() => import(/* webpackChunkName: 'deposit' */ '@/view/deposit/index.vue')
			},
			{
				path:'pay',
		    name:'pay',
				meta:{
		      title: 'Online Lottery - Play Lotto Online at Multilotto',
		      requireAuth: true
		    },
				component:() => import(/* webpackChunkName: 'deposit' */ '@/view/deposit/payment.vue')
			},
			{
				path:'complete',
		    name:'complete',
				meta:{
		      title: 'Online Lottery - Play Lotto Online at Multilotto',
		      requireAuth: true
		    },
				component:() => import(/* webpackChunkName: 'deposit' */ '@/view/deposit/complete.vue')
			}
		]
	},
	{
		path:'/promotions',
		name:'promotions',
		component: Main,
		children:[
			{
				path:'promotions',
		    name:'promotions',
				meta:{
	        title: 'Promotions'
	      },
			  component:() => import('@/view/promotions/index.vue')
			}
		]
	},
	{
		path:'/lotto',
		name:'lotto',
		component: Main,
		children:[
			{
				path:'lotto',
		    name:'lotto',
				meta:{
		      title: 'Play Scratchcards Online, Best Scratch Games'
		    },
		    component:() => import('@/view/lotto/index.vue')
			}
		]
	},
	{
		path:'/news',
		name:'news',
		component: Main,
		children:[
			{
			  path:'news',
			  name:'news',
				meta:{
		      title: 'Play Scratchcards Online, Best Scratch Games'
		    },
		    component:() => import('@/view/news/index.vue')
			},
			{
			  path:'newsdetail/:id',
			  name:'newsdetail',
				meta:{
		      title: 'Play Scratchcards Online, Best Scratch Games'
		    },
		    component:() => import('@/view/news/detail.vue')
			}
		]
	},
	{
		path:'/casino',
		name:'casino',
		component: Main,
		children:[
			{
			  path:'casino_page',
			  name:'casino_page',
				meta:{
		      title: 'The Best Online Casino - Biggest Games & Jackpot Slots'
		    },
		    component:() => import('@/view/casino/index.vue')
			},
			{
			  path:'casino_play/:id',
			  name:'casino_play',
			  meta:{
		      title: route => `Play ${route.params.id} Online`
		    },
		    component:() => import('@/view/casino/play.vue')
			},
			{
			  path:'casino_introduce/:id',
			  name:'casino_introduce',
			  meta:{
		      title: 'The Best Online Casino - Biggest Games & Jackpot Slots'
		    },
		    component:() => import('@/view/casino/introduce.vue')
			}
		]
	},
	{
		path:'/result',
		name:'result',
		component: Main,
		children:[
			{
				path:'result',
		    name:'result',
				meta:{
		      title: 'Play Scratchcards Online, Best Scratch Games',
		    },
		    component:() => import('@/view/result/index.vue')
			},
			// {
			// 	path:'resdetail/:name',
		 //    name:'resdetail',
			// 	meta:{
		 //      title: 'Play Scratchcards Online, Best Scratch Games',
		 //    },
		 //    component:() => import('@/view/result/detail.vue')
			// },
			// {
			// 	path:'resdetail/:name/:date',
		 //    name:'resdraw',
			// 	meta:{
		 //      title: 'Play Scratchcards Online, Best Scratch Games',
		 //    },
		 //    component:() => import('@/view/result/draw.vue')
			// }
			{
				path:'resdetail',
		    name:'resdetail',
				meta:{
		      title: 'Play Scratchcards Online, Best Scratch Games',
		    },
		    component:() => import('@/view/result/detail.vue')
			},
			{
				path:'resdraw',
		    name:'resdraw',
				meta:{
		      title: 'Play Scratchcards Online, Best Scratch Games',
		    },
		    component:() => import('@/view/result/draw.vue')
			}
		]
	},
	{
		path:'/game',
		name:'game',
		component: Main,
		children:[
			{
				path:'game',
		    name:'game',
				meta:{
		      title: 'Play Scratchcards Online, Best Scratch Games'
		    },
		    component:() => import('@/view/game/index.vue')
			}
		]
	},
	{
		path:'/play',
		name:'play',
		component: Main,
		children:[
			{
				path:'play',
		    name:'play',
				meta:{
		      title: 'how to play'
		    },
		    component:() => import('@/view/play/index.vue')
			}
		]
	},
	{
		path:'/quick',
		name:'quick',
		component: Main,
		children:[
			{
				path:'quick',
		    name:'quick',
				meta:{
		      title: 'Online Lottery - Play Lotto Online at Multilotto'
		    },
		    component:() => import('@/view/quick/index.vue')
			}
		]
	},
	{
		path:'/guide',
		name:'guide',
		component: Main,
		children:[
			{
				path:'guide',
		    name:'guide',
				component:() => import(/* webpackChunkName: 'playcenter' */ '@/view/guide/index.vue'),
				meta:{
		      title: 'Play Scratchcards Online, Best Scratch Games'
		    }
			},
			{
		  	path:'guidedetail',
			  name:'guidedetail',
				component:() => import(/* webpackChunkName: 'playcenter' */ '@/view/guide/detail.vue'),
				meta:{
		      title: 'Play Scratchcards Online, Best Scratch Games'
		    }
		  }
		]
	},
	{
		path:'/download',
		name:'download',
		component:() => import('@/view/download/index.vue'),
		meta:{
			keepAlive: true,
      title: 'Play Scratchcards Online, Best Scratch Games'
    }
	},
	{
		path:'/forgot',
		name:'forgot',
		component: Main,
		children:[
			{
				path:'forgot',
		    name:'forgot',
				meta:{
	       title: 'Forgot Password'
	      },
			  component:() => import('@/view/forgot/index.vue')
			}
		]
	},
	{
		path:'*',
		name:'error',
		component:() => import('@/view/error/index.vue'),
		meta: {
			keepAlive: true,
      hideInMenu: true,
      title: 'Play Scratchcards Online, Best Scratch Games'
    }
	}
]