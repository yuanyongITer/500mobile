
import Mock from 'mockjs'
import { localRead } from '@/lib/tools'

export const getBanList = () => {
	return Mock.mock({
		"code":0,
    "status":0,
		"data":[
			{
				"name": "Powerball",
				"user_currency": "€",
				"symbollocation":"before",
				"jackpot": "@float(1, 100, 1, 2)",
				"boost_jackpot": "@float(101, 500, 1, 2)",
				"draw_date": "2020-12-21 12:30:00",
				"logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/usa-powerball.png",
				"boost": true,
				"background": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/uploaded/5d1daeac80be6.jpg"
			},
			{
				"name": "Mega Millions",
				"user_currency": "€",
				"symbollocation":"before",
				"jackpot": "@float(1, 100, 1, 2)",
				"boost_jackpot": "@float(101, 500, 1, 2)",
				"draw_date": "2020-12-21 15:00:00",
				"logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/mega-millions.png",
				"boost": true,
				"background": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/uploaded/5d1dae6255866.jpg"
			},
			{
				"name": "eurojackpot",
				"user_currency": "€",
				"symbollocation":"before",
				"jackpot": "@float(1, 100, 1, 2)",
				"boost_jackpot": "@float(101, 500, 1, 2)",
				"draw_date": "2020-12-22 12:00:00",
				"logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/eurojackpot.png",
				"boost": false,
				"background": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/uploaded/5d1daecfa2442.jpg"
			}
		]
	})
}

export const getLanList = () => {
	return Mock.mock({
		"code":0,
    "status":0,
		"data":[
			{
        'name':'English',
        'Abbreviation':'EN',
        'code':'en-US'
      },
      {
        'name':'简体中文',
        'Abbreviation':'ZH-CN',
        'code':'zh-CN'
      },
      {
        'name':'English (Canada)',
        'Abbreviation':'CA',
        'code':'ca'
      },
      {
        'name':'Español',
        'Abbreviation':'ES',
        'code':'es'
      },
      {
        'name':'Italiano',
        'Abbreviation':'IT',
        'code':'it'
      },
      {
        'name':'Magyar',
        'Abbreviation':'HU',
        'code':'hu'
      },
      {
        'name':'Português',
        'Abbreviation':'PT',
        'code':'pt'
      },
      {
        'name':'Srpskohrvatski',
        'Abbreviation':'RS',
        'code':'rs'
      },
      {
        'name':'Suomi',
        'Abbreviation':'FI',
        'code':'fi'
      },
      {
        'name':'Svenska',
        'Abbreviation':'SE',
        'code':'se'
      },
      {
        'name':'Українська мова',
        'Abbreviation':'UA',
        'code':'ua'
      },
      {
        'name':'Deutsch',
        'Abbreviation':'DE',
        'code':'de'
      },
      {
        'name':'正體中文',
        'Abbreviation':'TW',
        'code':'tw'
      },
      {
        'name':'Tagalog',
        'Abbreviation':'TL',
        'code':'tl'
      }
		]
	})
}

export const getTicket = (option) => {
	let ticketInfo = {};
	let { ticket_id } = JSON.parse(option.body);
  switch (ticket_id) {
    case 'powerball':
      ticketInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name": "Powerball",
			    "user_currency": "€",
			    "symbollocation": "after",
			    "jackpot": 39.8,
			    "boost_jackpot": 355.6,
			    "draw_day": "2020-04-21",
			    "draw_date": "16 hours, 21 minutes",
			    "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/usa-powerball.png",
			    "boost": true,
			    "boost_open": false,
			    "unit_price": 3.5,
			    "unit_jackpot_price": 6.0,
			    "front_number": 69,
			    "front_pick_number": 5,
			    "back_number": 26,
			    "back_pick_number": 1
			  }
			});
      break;
    case 'eurojackpot':
      ticketInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name": "Eurojackpot",
			    "user_currency": "€",
			    "symbollocation":"before",
			    "jackpot": 90,
			    "draw_day": "2020-04-21",
			    "draw_date": "1 hours, 51 minutes",
			    "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/eurojackpot.png",
			    "boost": false,
			    "unit_price": 2.5,
			    "front_number": 50,
			    "front_pick_number": 5,
			    "back_number": 10,
			    "back_pick_number": 2
			  }
			});
      break;
    case 'euromillions':
      ticketInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name": "Euromillions",
			    "user_currency": "€",
			    "symbollocation":"before",
			    "jackpot": 32,
			    "draw_day": "2020-04-21",
			    "draw_date": "8 hours, 51 minutes",
			    "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/euromillions.png",
			    "boost": false,
			    "unit_price": 3.0,
			    "front_number": 50,
			    "front_pick_number": 5,
			    "back_number": 12,
			    "back_pick_number": 2
			  }
			});
      break;
    default:
      ticketInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name": "Euromillions",
			    "user_currency": "€",
			    "symbollocation":"before",
			    "jackpot": 28,
			    "draw_day": "2020-04-21",
			    "draw_date": "8 hours, 51 minutes",
			    "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/euromillions.png",
			    "boost": false,
			    "unit_price": 1,
			    "front_number": 50,
			    "front_pick_number": 5,
			    "back_number": 12,
			    "back_pick_number": 2
			  }
			});
  }
  return ticketInfo;
}

export const getPickList = () => {
	return Mock.mock({
		"code":0,
    "status":0,
		"data":[
			{
        'name':'Christmas Lottery',
        'id':'christmas-lottery',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/christmas-lottery.png',
        'jackpots':'500,000,000',
        'is_boost':true,
        'time':'0 days, 11 hours, 59 minutes left',
        'draws':[1,6],
        'draw_price':'6.0',
        'currency':'€',
        "front_number": 50,
		    "front_pick_number": 5,
		    "back_number": 12,
		    "back_pick_number": 2
      },
      {
        'name':'Lotto2020',
        'id':'year-lottery',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/year-lottery.png',
        'jackpots':'400,000,000',
        'is_boost':true,
        'time':'0 days, 11 hours, 56 minutes left',
        'draws':[1,6],
        'draw_price':'5.0',
        'currency':'€',
        "front_number": 50,
		    "front_pick_number": 5,
		    "back_number": 12,
		    "back_pick_number": 2
      },
      {
        'name':'Powerball',
        'id':'usa-powerball',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/usa-powerball.png',
        'jackpots':'338,100,000',
        'is_boost':true,
        'time':'1 day, 19 hours, 23 minutes left',
        'draws':[1,4,8,12],
        'draw_price':'6.0',
        'currency':'€',
        "front_number": 69,
		    "front_pick_number": 5,
		    "back_number": 26,
		    "back_pick_number": 1
      },
      {
        'name':'Mega Millions',
        'id':'mega-millions',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/mega-millions.png',
        'jackpots':'338,100,000',
        'is_boost':true,
        'time':'0 days, 19 hours, 22 minutes left',
        'draws':[1,4,8,12],
        'draw_price':'5.0',
        'currency':'€',
        "front_number": 50,
		    "front_pick_number": 5,
		    "back_number": 12,
		    "back_pick_number": 2
      },
      {
        'name':'Euromillions',
        'id':'euromillions',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/euromillions.png',
        'jackpots':'155,000,000',
        'is_boost':true,
        'time':'0 days, 11 hours, 21 minutes left',
        'draws':[1,4,8,12],
        'draw_price':'3.5',
        'currency':'€',
        "front_number": 50,
		    "front_pick_number": 5,
		    "back_number": 12,
		    "back_pick_number": 2
      },
      {
        'name':'SuperEnaLotto',
        'id':'super-ena-lotto',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/super-ena-lotto.png',
        'jackpots':'75,000,000',
        'is_boost':false,
        'time':'2 days, 10 hours, 20 minutes left',
        'draws':[1,3,6,12],
        'draw_price':'1.5',
        'currency':'€',
        "front_number": 50,
		    "front_pick_number": 5,
		    "back_number": 12,
		    "back_pick_number": 2
      },
      {
        'name':'Gold Lottery',
        'id':'gold-lottery',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/gold-lottery.png',
        'jackpots':'1 Ton of Gold',
        'is_boost':true,
        'time':'0 days, 11 hours, 46 minutes left',
        'draws':[1,6],
        'draw_price':'4.0',
        'currency':'€',
        "front_number": 50,
		    "front_pick_number": 5,
		    "back_number": 12,
		    "back_pick_number": 2
      },
      {
        'name':'Eurojackpot',
        'id':'eurojackpot',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/eurojackpot.png',
        'jackpots':'20,000,000',
        'is_boost':true,
        'time':'3 days, 10 hours, 14 minutes left',
        'draws':[1,2,6,12],
        'draw_price':'2.0',
        'currency':'€',
        "front_number": 50,
		    "front_pick_number": 5,
		    "back_number": 10,
		    "back_pick_number": 2
      },
      {
        'name':'Finland Lotto',
        'id':'finland-lotto',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/finland-lotto.png',
        'jackpots':'20,000,000',
        'is_boost':true,
        'time':'4 days, 10 hours, 58 minutes left',
        'draws':[1,4,8,12],
        'draw_price':'3.0',
        'currency':'€',
        "front_number": 50,
		    "front_pick_number": 5,
		    "back_number": 12,
		    "back_pick_number": 2
      },
      {
        'name':'Germany Lotto',
        'id':'germany-lotto',
        'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/md/germany-lotto.png',
        'jackpots':'6,000,000',
        'is_boost':false,
        'time':'1 day, 8 hours, 36 minutes left',
        'draws':[1,4,8,12],
        'draw_price':'3.0',
        'currency':'€',
        "front_number": 50,
		    "front_pick_number": 5,
		    "back_number": 12,
		    "back_pick_number": 2
      }
		]
	})
}

export const getDrawList = () => {
	return Mock.mock({
		"code":0,
    "status":0,
    "data":[
      {
				"onedatemoney": "\u20ac441",
				"apponedatepayout": "<80>440.53",
				"onedatepayout": "440.53",
				"firstname": "P**",
				"lastname": "K**",
				"gamename": "Norway Lotto",
				"gametype": "Lotto",
				"appfirstname": "P**",
				"date": "2020\/01\/11",
				"gametypeid": "17",
				"lotteryid":"1001"
			}, {
				"onedatemoney": "\u20ac441",
				"apponedatepayout": "<80>440.53",
				"onedatepayout": "440.53",
				"firstname": "A**",
				"lastname": "M**",
				"gamename": "Norway Lotto",
				"gametype": "Lotto",
				"appfirstname": "A**",
				"date": "2020\/01\/11",
				"gametypeid": "17",
				"lotteryid":"1002"
			}, {
				"onedatemoney": "\u20ac255",
				"apponedatepayout": "<80>254.70",
				"onedatepayout": "254.7",
				"firstname": "A**",
				"lastname": "R**",
				"gamename": "Germany Lotto",
				"gametype": "Lotto",
				"appfirstname": "A**",
				"date": "2020\/01\/13",
				"gametypeid": "28",
				"lotteryid":"1003"
			}, {
				"onedatemoney": "\u20ac220",
				"apponedatepayout": "<80>220.00",
				"onedatepayout": "220.0",
				"firstname": "K**",
				"lastname": "A**",
				"gamename": "Cash4Life",
				"gametype": "Lotto",
				"appfirstname": "K**",
				"date": "2020\/01\/14",
				"gametypeid": "58",
				"lotteryid":"1004"
			}, {
				"onedatemoney": "\u20ac369",
				"apponedatepayout": "<80>369.30",
				"onedatepayout": "369.3",
				"firstname": "M**",
				"lastname": "P**",
				"gamename": "Norway Lotto",
				"gametype": "Lotto",
				"appfirstname": "M**",
				"date": "2020\/01\/18",
				"gametypeid": "17",
				"lotteryid":"1005"
			}, {
				"onedatemoney": "\u20ac10,198",
				"apponedatepayout": "<80>10197.80",
				"onedatepayout": "10197.8",
				"firstname": "S**",
				"lastname": "S**",
				"gamename": "Germany Lotto",
				"gametype": "Lotto",
				"appfirstname": "S**",
				"date": "2020\/01\/20",
				"gametypeid": "28",
				"lotteryid":"1006"
			}, {
				"onedatemoney": "\u20ac476",
				"apponedatepayout": "<80>476.47",
				"onedatepayout": "476.47",
				"firstname": "S**",
				"lastname": "V**",
				"gamename": "Norway Lotto",
				"gametype": "Lotto",
				"appfirstname": "S**",
				"date": "2020\/01\/25",
				"gametypeid": "17",
				"lotteryid":"1007"
			}, {
				"onedatemoney": "\u20ac362",
				"apponedatepayout": "<80>362.20",
				"onedatepayout": "362.2",
				"firstname": "M**",
				"lastname": "P**",
				"gamename": "Norway Lotto",
				"gametype": "Lotto",
				"appfirstname": "M**",
				"date": "2020\/01\/25",
				"gametypeid": "17",
				"lotteryid":"1008"
			}, {
				"onedatemoney": "\u20ac981",
				"apponedatepayout": "<80>981.31",
				"onedatepayout": "981.31",
				"firstname": "S**",
				"lastname": "S**",
				"gamename": "Bonoloto",
				"gametype": "Lotto",
				"appfirstname": "S**",
				"date": "2020\/02\/11",
				"gametypeid": "24",
				"lotteryid":"1009"
			}, {
				"onedatemoney": "\u20ac406",
				"apponedatepayout": "<80>405.70",
				"onedatepayout": "405.7",
				"firstname": "Y**",
				"lastname": "T**",
				"gamename": "Norway Lotto",
				"gametype": "Lotto",
				"appfirstname": "Y**",
				"date": "2020\/02\/29",
				"gametypeid": "17",
				"lotteryid":"1010"
			}, {
				"onedatemoney": "\u20ac3,542",
				"apponedatepayout": "<80>3541.70",
				"onedatepayout": "3541.7",
				"firstname": "P**",
				"lastname": "B**",
				"gamename": "Germany Lotto",
				"gametype": "Lotto",
				"appfirstname": "P**",
				"date": "2020\/03\/02",
				"gametypeid": "28",
				"lotteryid":"1011"
			}, {
				"onedatemoney": "\u20ac241",
				"apponedatepayout": "<80>241.00",
				"onedatepayout": "241.0",
				"firstname": "A**",
				"lastname": "D**",
				"gamename": "Germany Lotto",
				"gametype": "Lotto",
				"appfirstname": "A**",
				"date": "2020\/03\/05",
				"gametypeid": "28",
				"lotteryid":"1012"
			}
		]
	})
}

export const getGameInfo = () => {
	return Mock.mock({
    "status":0,
    "code":0,
		"data":[
	    {
	     "gametype":0,
	     "list": [
	        {
	          "name": "Golden Ace",
	          "def_tickets": 5,
	          "def_percentage": 0,
	          "ticket_value": 5,
	          "tickets_batches_id": "1",
	          "currentjackpot": "50000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/golden-ace.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5e60dda868dbf.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/golden-ace",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/golden-ace",
	          "play_bt_str": "Play - \u20ac25.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac50 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli   data-quantity=\"1\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"5\" data-amount-str=\"\u20ac25.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E5 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac25.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"10\" data-amount-str=\"\u20ac50.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac50.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "St.Patrick's Day",
	          "def_tickets": 5,
	          "def_percentage": 0,
	          "ticket_value": 1,
	          "tickets_batches_id": "1",
	          "currentjackpot": "100000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/happy-stpatrick.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5e60db290bf40.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/happy-stpatrick",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/happy-stpatrick",
	          "play_bt_str": "Play - \u20ac5.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac100 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli  data-quantity=\"1\" data-amount-str=\"\u20ac1.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac1.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"5\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E5 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"10\" data-amount-str=\"\u20ac10.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac10.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "777",
	          "def_tickets": 10,
	          "def_percentage": 0,
	          "ticket_value": 0.5,
	          "tickets_batches_id": "7",
	          "currentjackpot": "7777",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/lucky-777.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d088093c52bd.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/lucky-777",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/lucky-777",
	          "play_bt_str": "Play - \u20ac5.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac7 777.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli   data-quantity=\"1\" data-amount-str=\"\u20ac0.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"10\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"20\" data-amount-str=\"\u20ac10.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac10.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Fruity Flurry",
	          "def_tickets": 10,
	          "def_percentage": 0,
	          "ticket_value": 0.8,
	          "tickets_batches_id": "7",
	          "currentjackpot": "50000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/fruity-flurry.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d07654eae4ed.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/fruity-flurry",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/fruity-flurry",
	          "play_bt_str": "Play - \u20ac8.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac50 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli   data-quantity=\"1\" data-amount-str=\"\u20ac0.80\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.80\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"10\" data-amount-str=\"\u20ac8.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac8.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"20\" data-amount-str=\"\u20ac16.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac16.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "33Chances",
	          "def_tickets": 5,
	          "def_percentage": 0,
	          "ticket_value": 2,
	          "tickets_batches_id": "1",
	          "currentjackpot": "20000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/33Chances.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d076471b4cc7.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/33Chances",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/33Chances",
	          "play_bt_str": "Play - \u20ac10.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac20 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli   data-quantity=\"1\" data-amount-str=\"\u20ac2.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac2.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"5\" data-amount-str=\"\u20ac10.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E5 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac10.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"10\" data-amount-str=\"\u20ac20.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac20.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Bichomania",
	          "def_tickets": 10,
	          "def_percentage": 0,
	          "ticket_value": 0.4,
	          "tickets_batches_id": "7",
	          "currentjackpot": "4000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/bichomania.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d0762d1096ea.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/bichomania",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/bichomania",
	          "play_bt_str": "Play - \u20ac4.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac4 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli   data-quantity=\"1\" data-amount-str=\"\u20ac0.40\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.40\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"10\" data-amount-str=\"\u20ac4.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac4.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"20\" data-amount-str=\"\u20ac8.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac8.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Elephant Scratch",
	          "def_tickets": 20,
	          "def_percentage": 0,
	          "ticket_value": 0.05,
	          "tickets_batches_id": "8",
	          "currentjackpot": "500",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/elephant-scratch.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d07620b77715.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/elephant-scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/elephant-scratch",
	          "play_bt_str": "Play - \u20ac1.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac500.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli   data-quantity=\"10\" data-amount-str=\"\u20ac0.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"20\" data-amount-str=\"\u20ac1.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac1.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"50\" data-amount-str=\"\u20ac2.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E50 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac2.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Double Chances S",
	          "def_tickets": 10,
	          "def_percentage": 0,
	          "ticket_value": 0.5,
	          "tickets_batches_id": "7",
	          "currentjackpot": "10000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/Double-Chances-S.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d0760d3f2e1e.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/Double-Chances-S",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/Double-Chances-S",
	          "play_bt_str": "Play - \u20ac5.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac10 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli   data-quantity=\"1\" data-amount-str=\"\u20ac0.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"10\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"20\" data-amount-str=\"\u20ac10.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac10.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Double Chances M",
	          "def_tickets": 5,
	          "def_percentage": 0,
	          "ticket_value": 2.5,
	          "tickets_batches_id": "1",
	          "currentjackpot": "50000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/Double-Chances-M.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d075faccc92f.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/Double-Chances-M",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/Double-Chances-M",
	          "play_bt_str": "Play - \u20ac12.50",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac50 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli   data-quantity=\"1\" data-amount-str=\"\u20ac2.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac2.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"5\" data-amount-str=\"\u20ac12.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E5 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac12.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"10\" data-amount-str=\"\u20ac25.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac25.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Horseshoe Scratch",
	          "def_tickets": 20,
	          "def_percentage": 0,
	          "ticket_value": 0.1,
	          "tickets_batches_id": "8",
	          "currentjackpot": "1000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/horseshoe-scratch.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d075dc5478f0.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/horseshoe-scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/horseshoe-scratch",
	          "play_bt_str": "Play - \u20ac2.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac1 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli   data-quantity=\"10\" data-amount-str=\"\u20ac1.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac1.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"on\"  data-quantity=\"20\" data-amount-str=\"\u20ac2.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac2.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli   data-quantity=\"50\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E50 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }
	      ]
	    },
	    {
	     "gametype":1,
	      "list": [
	        {
	          "name": "Golden Ace",
	          "def_tickets": 5,
	          "def_percentage": 0,
	          "ticket_value": 5,
	          "tickets_batches_id": "1",
	          "currentjackpot": "50000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/golden-ace.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5e60dda868dbf.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/golden-ace",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/golden-ace",
	          "play_bt_str": "Play - \u20ac25.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac50 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"1\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"5\" data-amount-str=\"\u20ac25.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E5 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac25.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"10\" data-amount-str=\"\u20ac50.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac50.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "St.Patrick's Day",
	          "def_tickets": 5,
	          "def_percentage": 0,
	          "ticket_value": 1,
	          "tickets_batches_id": "1",
	          "currentjackpot": "100000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/happy-stpatrick.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5e60db290bf40.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/happy-stpatrick",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/happy-stpatrick",
	          "play_bt_str": "Play - \u20ac5.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac100 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"1\" data-amount-str=\"\u20ac1.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac1.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"5\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E5 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"10\" data-amount-str=\"\u20ac10.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac10.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "777",
	          "def_tickets": 10,
	          "def_percentage": 0,
	          "ticket_value": 0.5,
	          "tickets_batches_id": "7",
	          "currentjackpot": "7777",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/lucky-777.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d088093c52bd.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/lucky-777",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/lucky-777",
	          "play_bt_str": "Play - \u20ac5.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac7 777.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"1\" data-amount-str=\"\u20ac0.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"10\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"20\" data-amount-str=\"\u20ac10.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac10.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Fruity Flurry",
	          "def_tickets": 10,
	          "def_percentage": 0,
	          "ticket_value": 0.8,
	          "tickets_batches_id": "7",
	          "currentjackpot": "50000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/fruity-flurry.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d07654eae4ed.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/fruity-flurry",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/fruity-flurry",
	          "play_bt_str": "Play - \u20ac8.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac50 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"1\" data-amount-str=\"\u20ac0.80\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.80\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"10\" data-amount-str=\"\u20ac8.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac8.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"20\" data-amount-str=\"\u20ac16.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac16.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "33Chances",
	          "def_tickets": 5,
	          "def_percentage": 0,
	          "ticket_value": 2,
	          "tickets_batches_id": "1",
	          "currentjackpot": "20000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/33Chances.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d076471b4cc7.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/33Chances",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/33Chances",
	          "play_bt_str": "Play - \u20ac10.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac20 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"1\" data-amount-str=\"\u20ac2.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac2.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"5\" data-amount-str=\"\u20ac10.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E5 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac10.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"10\" data-amount-str=\"\u20ac20.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac20.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Super Shamrock",
	          "def_tickets": 20,
	          "def_percentage": 0,
	          "ticket_value": 0.15,
	          "tickets_batches_id": "8",
	          "currentjackpot": "1500",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/super-shamrock.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d03b39b64b77.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/super-shamrock",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/super-shamrock",
	          "play_bt_str": "Play - \u20ac3.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac1 500.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"10\" data-amount-str=\"\u20ac1.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac1.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"20\" data-amount-str=\"\u20ac3.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac3.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"50\" data-amount-str=\"\u20ac7.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E50 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac7.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }
	      ]
	    },
	    {
	     "gametype":3,
	     "list": [
	        {
	          "name": "Bichomania",
	          "def_tickets": 10,
	          "def_percentage": 0,
	          "ticket_value": 0.4,
	          "tickets_batches_id": "7",
	          "currentjackpot": "4000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/bichomania.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d0762d1096ea.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/bichomania",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/bichomania",
	          "play_bt_str": "Play - \u20ac4.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac4 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"1\" data-amount-str=\"\u20ac0.40\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.40\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"10\" data-amount-str=\"\u20ac4.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac4.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"20\" data-amount-str=\"\u20ac8.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac8.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Elephant Scratch",
	          "def_tickets": 20,
	          "def_percentage": 0,
	          "ticket_value": 0.05,
	          "tickets_batches_id": "8",
	          "currentjackpot": "500",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/elephant-scratch.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d07620b77715.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/elephant-scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/elephant-scratch",
	          "play_bt_str": "Play - \u20ac1.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac500.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"10\" data-amount-str=\"\u20ac0.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"20\" data-amount-str=\"\u20ac1.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac1.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"50\" data-amount-str=\"\u20ac2.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E50 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac2.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Double Chances S",
	          "def_tickets": 10,
	          "def_percentage": 0,
	          "ticket_value": 0.5,
	          "tickets_batches_id": "7",
	          "currentjackpot": "10000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/Double-Chances-S.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d0760d3f2e1e.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/Double-Chances-S",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/Double-Chances-S",
	          "play_bt_str": "Play - \u20ac5.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac10 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"1\" data-amount-str=\"\u20ac0.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"10\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"20\" data-amount-str=\"\u20ac10.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac10.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Horseshoe Scratch",
	          "def_tickets": 20,
	          "def_percentage": 0,
	          "ticket_value": 0.1,
	          "tickets_batches_id": "8",
	          "currentjackpot": "1000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/horseshoe-scratch.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d075dc5478f0.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/horseshoe-scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/horseshoe-scratch",
	          "play_bt_str": "Play - \u20ac2.00",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac1 000.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"10\" data-amount-str=\"\u20ac1.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac1.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"20\" data-amount-str=\"\u20ac2.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac2.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"50\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E50 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }, {
	          "name": "Raid the Piggy Bank",
	          "def_tickets": 10,
	          "def_percentage": 0,
	          "ticket_value": 0.25,
	          "tickets_batches_id": "7",
	          "currentjackpot": "2500",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/gluck\/en\/raid-the-piggy-bank.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d075b932bfbd.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/raid-the-piggy-bank",
	          "play_url": "https:\/\/www.multilotto.com\/en\/scratchcards\/buytickets\/raid-the-piggy-bank",
	          "play_bt_str": "Play - \u20ac2.50",
	          "play_bt": "Play",
	          "currentjackpot_str": "WIN UP TO \u20ac2 500.00",
	          "play_scratchcard_str": "",
	          "buy_scratchcard_str": "\u003Cul class=\"gluck-card-list\"\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"1\" data-amount-str=\"\u20ac0.25\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E1 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac0.25\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check on\"  data-quantity=\"10\" data-amount-str=\"\u20ac2.50\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E10 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac2.50\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003Cli class=\"js-card-check \"  data-quantity=\"20\" data-amount-str=\"\u20ac5.00\"\u003E\u003Cdiv class=\"line-container\"\u003E\u003Cdiv class=\"linebox\"\u003E\u003Cb class=\"radio\"\u003E\u003C\/b\u003E\u003Cspan class=\"quantity\"\u003E20 game \u003C\/span\u003E\u003C\/div\u003E\n\u003Cdiv class=\"linebox\"\u003E \u20ac5.00\u003C\/div\u003E\u003C\/div\u003E\u003C\/li\u003E\u003C\/ul\u003E"
	        }
	      ]
	    },
	    {
	     "gametype":5,
	     "list": [
	        {
	          "name": "Double Salary 1 Year",
	          "per_price_currency": "\u20ac2.00",
	          "currentjackpot": "30000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5dc53445cfeaf.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5db9b5adcb0b8.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/doublesalaryoneyear",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/doublesalaryoneyear",
	          "currentjackpot_str": "WIN UP TO \u20ac30 000.00",
	          "play_str": "Play - \u20ac2.00",
	          "per_price_currency_str": "\u20ac2.00"
	        }, {
	          "name": "Cash Vault I",
	          "per_price_currency": "\u20ac1.00",
	          "currentjackpot": "30000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47b154a8f34.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47b154cd30d.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/cash_vault_I_scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/cash_vault_I_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac30 000.00",
	          "play_str": "Play - \u20ac1.00",
	          "per_price_currency_str": "\u20ac1.00"
	        }, {
	          "name": "Cash Vault II",
	          "per_price_currency": "\u20ac2.00",
	          "currentjackpot": "60000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d4801de67f6e.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d4801de8c321.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/cash_vault_II_scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/cash_vault_II_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac60 000.00",
	          "play_str": "Play - \u20ac2.00",
	          "per_price_currency_str": "\u20ac2.00"
	        }, {
	          "name": "Diamond Rush",
	          "per_price_currency": "\u20ac0.75",
	          "currentjackpot": "30000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47b01e65458.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47b01e8ca75.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/diamond_rush",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/diamond_rush",
	          "currentjackpot_str": "WIN UP TO \u20ac30 000.00",
	          "play_str": "Play - \u20ac0.75",
	          "per_price_currency_str": "\u20ac0.75"
	        }, {
	          "name": "Dream Car SUV",
	          "per_price_currency": "\u20ac5.00",
	          "currentjackpot": "75000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47afc3d2b4b.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47afc416fc1.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/dream_car_suv_scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/dream_car_suv_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac75 000.00",
	          "play_str": "Play - \u20ac5.00",
	          "per_price_currency_str": "\u20ac5.00"
	        }, {
	          "name": "Dream Car Urban",
	          "per_price_currency": "\u20ac2.50",
	          "currentjackpot": "25000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47ab923b712.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47ab925a1ae.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/dream_car_urban_scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/dream_car_urban_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac25 000.00",
	          "play_str": "Play - \u20ac2.50",
	          "per_price_currency_str": "\u20ac2.50"
	        }, {
	          "name": "Gold Rush",
	          "per_price_currency": "\u20ac0.25",
	          "currentjackpot": "10000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47ab0849e2c.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47ab086d9ea.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/gold_rush_scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/gold_rush_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac10 000.00",
	          "play_str": "Play - \u20ac0.25",
	          "per_price_currency_str": "\u20ac0.25"
	        }, {
	          "name": "Happy Scratch",
	          "per_price_currency": "\u20ac0.10",
	          "currentjackpot": "1000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47aa52b639b.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47aa52e3d1e.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/happy_scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/happy_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac1 000.00",
	          "play_str": "Play - \u20ac0.10",
	          "per_price_currency_str": "\u20ac0.10"
	        }, {
	          "name": "It\u2019s Bananas",
	          "per_price_currency": "\u20ac1.00",
	          "currentjackpot": "75000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47a84d03157.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47a84d2a43d.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/its_bananas",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/its_bananas",
	          "currentjackpot_str": "WIN UP TO \u20ac75 000.00",
	          "play_str": "Play - \u20ac1.00",
	          "per_price_currency_str": "\u20ac1.00"
	        }, {
	          "name": "King Treasure",
	          "per_price_currency": "\u20ac0.75",
	          "currentjackpot": "30000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47a7a5c23c5.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47a7a5e997f.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/king_treasure_scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/king_treasure_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac30 000.00",
	          "play_str": "Play - \u20ac0.75",
	          "per_price_currency_str": "\u20ac0.75"
	        }
	      ]
	    },
	    {
	     "gametype":6,
	     "list": [
	        {
	          "name": "Frogs Scratch",
	          "per_price_currency": "\u20ac2.00",
	          "currentjackpot": "30000",
	          "user_currency": "EUR",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/casino/thumbnails/5e8ed98b75ebb.png",
	          "background": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/casino/backgrounds/5e8ed98ba5883.png",
	          "demo_url": "https://www.multilotto.com/en/casino/play/frogs_scratch",
	          "play_url": "https://www.multilotto.com/en/casino/play/frogs_scratch",
	          "currentjackpot_str": "WIN UP TO €250 000.00",
	          "play_str": "Play - \u20ac2.00",
	          "per_price_currency_str": "\u20ac2.00"
	        }, {
	          "name": "Cash Vault II",
	          "per_price_currency": "\u20ac2.00",
	          "currentjackpot": "60000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d4801de67f6e.jpg",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d4801de8c321.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/cash_vault_II_scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/cash_vault_II_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac60 000.00",
	          "play_str": "Play - \u20ac2.00",
	          "per_price_currency_str": "\u20ac2.00"
	        }, {
	          "name": "King Treasure",
	          "per_price_currency": "\u20ac0.75",
	          "currentjackpot": "30000",
	          "user_currency": "EUR",
	          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47a7a5c23c5.png",
	          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47a7a5e997f.png",
	          "demo_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/king_treasure_scratch",
	          "play_url": "https:\/\/www.multilotto.com\/en\/casino\/play\/king_treasure_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac30 000.00",
	          "play_str": "Play - \u20ac0.75",
	          "per_price_currency_str": "\u20ac0.75"
	        }, {
	          "name": "Lucky Numbers x8",
	          "per_price_currency": "\u20ac0.75",
	          "currentjackpot": "30000",
	          "user_currency": "EUR",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/casino/thumbnails/5d47a6f3def66.png",
	          "background": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/casino/backgrounds/5d47a6f40cadb.png",
	          "demo_url": "https://www.multilotto.com/en/casino/play/lucky_lumbers8_scratch",
	          "play_url": "https://www.multilotto.com/en/casino/play/lucky_lumbers8_scratch",
	          "currentjackpot_str": "WIN UP TO \u20ac30 000.00",
	          "play_str": "Play - \u20ac0.75",
	          "per_price_currency_str": "\u20ac0.75"
	        }
	      ]
	    }
	  ]
	})
}

export const getGameMenu = () => {
	return Mock.mock({
    "status":0,
    "code":0,
		"data":[
	    {
	      "name":"All",
	      "gametype":0
	    },
	    {
	      "name":"Popular",
	      "gametype":1
	    },
	    {
	      "name":"Budget",
	      "gametype":3
	    },
	    {
	      "name":"Special",
	      "gametype":5
	    }
	  ]
	})
}

export const getNewsInfo = () => {
	return Mock.mock({
		"code":0,
    "status":0,
  	"data":[
		  {
	      "id":"1001",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71.png",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71-thumb.png",
				"title":"How Multilotto App Can Help You Hit the Jackpot: The Best Lottery App for iPhone and Android",
				"describe":"Is there a good lottery app out there that can help me buy tickets and check the winning n...",
				"time":"2020-03-17"
			},
			{
	      "id":"1002",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42.png",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42-thumb.png",
	      "title":"I Won the Lottery, What Happens Next? - How to Claim Your Prize",
	      "describe":"Did you just win a big prize in the lottery, or are you still dreaming about hitting a big jackpot in your ...",
	      "time":"2020-03-10"
	    },
	    {
	      "id":"1003",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013.jpg",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013-thumb.jpg",
	      "title":"Everything you need to know about the Spanish lottery El Gordo!",
	      "describe":"There are dozens of lucrative lotteries around the world those mint winners and there’s E...",
	      "time":"2020-02-27"
	    },
	    {
	      "id":"1004",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e1d3f13ce5a9.jpg",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e1d3f13ce5a9-thumb.png",
	      "title":"The first winner of 2020 hit €11114.20 on the German Lotto!",
	      "describe":"Happy New Year everyone, and a big congratulations to Anette R. from Germany that hit a massive €11114.20 win on the German Lotto 6aus49 on the 2nd of January 2020. What a way to start the...",
	      "time":"2020-01-14"
	    },
	    {
	      "id":"1005",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71.png",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71-thumb.png",
				"title":"How Multilotto App Can Help You Hit the Jackpot: The Best Lottery App for iPhone and Android",
				"describe":"Is there a good lottery app out there that can help me buy tickets and check the winning n...",
				"time":"2020-03-17"
			},
			{
	      "id":"1006",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42.png",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42-thumb.png",
	      "title":"I Won the Lottery, What Happens Next? - How to Claim Your Prize",
	      "describe":"Did you just win a big prize in the lottery, or are you still dreaming about hitting a big jackpot in your ...",
	      "time":"2020-03-10"
	    },
	    {
	      "id":"1007",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013.jpg",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013-thumb.jpg",
	      "title":"Everything you need to know about the Spanish lottery El Gordo!",
	      "describe":"There are dozens of lucrative lotteries around the world those mint winners and there’s E...",
	      "time":"2020-02-27"
	    },
	    {
	      "id":"1008",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e1d3f13ce5a9.jpg",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e1d3f13ce5a9-thumb.png",
	      "title":"The first winner of 2020 hit €11114.20 on the German Lotto!",
	      "describe":"Happy New Year everyone, and a big congratulations to Anette R. from Germany that hit a massive €11114.20 win on the German Lotto 6aus49 on the 2nd of January 2020. What a way to start the...",
	      "time":"2020-01-14"
	    },
	    {
	      "id":"1009",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71.png",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71-thumb.png",
				"title":"How Multilotto App Can Help You Hit the Jackpot: The Best Lottery App for iPhone and Android",
				"describe":"Is there a good lottery app out there that can help me buy tickets and check the winning n...",
				"time":"2020-03-17"
			},
			{
	      "id":"1010",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42.png",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42-thumb.png",
	      "title":"I Won the Lottery, What Happens Next? - How to Claim Your Prize",
	      "describe":"Did you just win a big prize in the lottery, or are you still dreaming about hitting a big jackpot in your ...",
	      "time":"2020-03-10"
	    },
	    {
	      "id":"1011",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71.png",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71-thumb.png",
				"title":"How Multilotto App Can Help You Hit the Jackpot: The Best Lottery App for iPhone and Android",
				"describe":"Is there a good lottery app out there that can help me buy tickets and check the winning n...",
				"time":"2020-03-17"
			},
			{
	      "id":"1012",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42.png",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42-thumb.png",
	      "title":"I Won the Lottery, What Happens Next? - How to Claim Your Prize",
	      "describe":"Did you just win a big prize in the lottery, or are you still dreaming about hitting a big jackpot in your ...",
	      "time":"2020-03-10"
	    },
	    {
	      "id":"1013",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013.jpg",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013-thumb.jpg",
	      "title":"Everything you need to know about the Spanish lottery El Gordo!",
	      "describe":"There are dozens of lucrative lotteries around the world those mint winners and there’s E...",
	      "time":"2020-02-27"
	    },
	    {
	      "id":"1014",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e1d3f13ce5a9.jpg",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e1d3f13ce5a9-thumb.png",
	      "title":"The first winner of 2020 hit €11114.20 on the German Lotto!",
	      "describe":"Happy New Year everyone, and a big congratulations to Anette R. from Germany that hit a massive €11114.20 win on the German Lotto 6aus49 on the 2nd of January 2020. What a way to start the...",
	      "time":"2020-01-14"
	    },
	    {
	      "id":"1015",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71.png",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71-thumb.png",
				"title":"How Multilotto App Can Help You Hit the Jackpot: The Best Lottery App for iPhone and Android",
				"describe":"Is there a good lottery app out there that can help me buy tickets and check the winning n...",
				"time":"2020-03-17"
			},
			{
	      "id":"1016",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e1d3f13ce5a9.jpg",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e1d3f13ce5a9-thumb.png",
	      "title":"The first winner of 2020 hit €11114.20 on the German Lotto!",
	      "describe":"Happy New Year everyone, and a big congratulations to Anette R. from Germany that hit a massive €11114.20 win on the German Lotto 6aus49 on the 2nd of January 2020. What a way to start the...",
	      "time":"2020-01-14"
	    },
	    {
	      "id":"1017",
	      "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013.jpg",
	      "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013-thumb.jpg",
	      "title":"Everything you need to know about the Spanish lottery El Gordo!",
	      "describe":"There are dozens of lucrative lotteries around the world those mint winners and there’s E...",
	      "time":"2020-02-27"
	    },
		]
	})
}

export const getNewsDetail = (option) => {
	let article = {};
	let { news_id } = JSON.parse(option.body);
  switch (news_id) {
    case '1001':
      article = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"id":"1001",
			    "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71.png",
			    "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e7071f431d71-thumb.png",
					"title":"How Multilotto App Can Help You Hit the Jackpot: The Best Lottery App for iPhone and Android",
					"describe":"Is there a good lottery app out there that can help me buy tickets and check the winning n...",
					"time":"2020-03-17",
			    "text":"It doesn’t matter if this is your first time buying a lottery ticket or you have been playing for years, everyone wants to win the lottery. Though, there are plenty of things that you need to unravel to answer this literal million-dollar question. Betting on the outcome of the lottery takes more than just luck. For winning the lottery, you need to come up with a foolproof strategy and work on it. Well, in this post, you’re going to learn something about how to be a lottery winner! After getting yourself familiar with the rules of the Power Ball jackpot (or any other jackpot), you should know about your fair chances. After all, it’s all in the math! You can either calculate it yourself or do some research to know the probability of winning the lottery. According to experts, we should go with the lottery where there is a higher probability of winning."
			  }
			});
      break;
    case '1002':
      article = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"id":"1002",
			    "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42.png",
			    "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e69aa3040a42-thumb.png",
			    "title":"I Won the Lottery, What Happens Next? - How to Claim Your Prize",
			    "describe":"Did you just win a big prize in the lottery, or are you still dreaming about hitting a big jackpot in your ...",
			    "time":"2020-03-10",
			    "text":"There are still a lot of people who don’t know about the range of lottery apps out there. To make it easier for everyone to have fun from online lottery, plenty of lotto apps have been developed. Needless to say, not every lottery number generator or app would fulfill your needs. Here are some of the reasons why Multilotto has been the most reliable lottery app that helps us to"
			  }
			});
      break;
    case '1003':
      article = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"id":"1003",
			    "maxlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013.jpg",
			    "minlogo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/articleimages/5e57991309013-thumb.jpg",
			    "title":"Everything you need to know about the Spanish lottery El Gordo!",
			    "describe":"There are dozens of lucrative lotteries around the world those mint winners and there’s E...",
			    "time":"2020-02-27",
			    "text":"There are still a lot of people who don’t know about the range of lottery apps out there. To make it easier for everyone to have fun from online lottery, plenty of lotto apps have been developed. Needless to say, not every lottery number generator or app would fulfill your needs. Here are some of the reasons why Multilotto has been the most reliable lottery app that helps us to bet on the outcome of all kinds and even keep us updated with the results. When we think of having fun from online lottery, this is the first problem that we face. To make it easier for us to select random numbers, Multilotto has included an inbuilt lottery number generator. The application has its own lucky number generator that takes the number formula and the lottery rules under consideration. By performing a quick lottery analytics, it will generate random numbers, complying with all the norms of the lottery. Therefore, you would no longer have to spend hours picking the right numbers and can just give its lottery number generator a try instead. A lot of users make the common mistake of simply getting a lottery ticket without knowing the terms and conditions of it. One of the best things about Multilotto is that it is extremely transparent and will let you know about the norms of the lottery in advance. Besides that, you would also stay up to date regarding the payout options, jackpot, and more. If you want, you can just turn on the option to get notified of the winners and other updates."
			  }
			});
      break;
  }

  return article;
}

export const getPlayInfo = () => {
	return Mock.mock({
		"status":0,
	  "code":0,
		"data":{
			"state": [
				{
					"name":"Europe",
			    "type":0,
			    "isDefault":true
				},
				{
			    "name":"North America",
			    "type":1,
			    "isDefault":false
				},
				{
			    "name":"Oceania",
			    "type":2,
			    "isDefault":false
				},
				{
			    "name":"South America",
			    "type":3,
			    "isDefault":false
				}
			],
			"nation":[
			  {
					"type":0,
					"list":[
					  {
							"isDefault":true,
							"country":"ALL",
							"name":"ALL"
						},
						{
							"isDefault":false,
							"country":"Sweden",
							"name":"Sweden"
						},
						{
							"isDefault":false,
							"country":"Germany",
							"name":"Germany"
						},
						{
							"isDefault":false,
							"country":"UK",
							"name":"UK"
						},
						{
							"isDefault":false,
							"country":"Finland",
							"name":"Finland"
						},
						{
							"isDefault":false,
							"country":"Ireland",
							"name":"Ireland"
						},
						{
							"isDefault":false,
							"country":"Switzerland",
							"name":"Switzerland"
						},
						{
							"isDefault":false,
							"country":"Spain",
							"name":"Spain"
						},
						{
							"isDefault":false,
							"country":"Austria",
							"name":"Austria"
						},
						{
							"isDefault":false,
							"country":"Norway",
							"name":"Norway"
						},
						{
							"isDefault":false,
							"country":"Poland",
							"name":"Poland"
						},
						{
							"isDefault":false,
							"country":"Belgium",
							"name":"Belgium"
						},
						{
							"isDefault":false,
							"country":"Italy",
							"name":"Italy"
						},
						{
							"isDefault":false,
							"country":"Denmark",
							"name":"Denmark"
						},
						{
							"isDefault":false,
							"country":"Estonia",
							"name":"Estonia"
						},
						{
							"isDefault":false,
							"country":"Iceland",
							"name":"Iceland"
						},
						{
							"isDefault":false,
							"country":"Latvia",
							"name":"Latvia"
						},
						{
							"isDefault":false,
							"country":"Lithuania",
							"name":"Lithuania"
						}
					]
				},
				{
					"type":1,
					"list":[
					  {
							"isDefault":true,
							"country":"ALL",
							"name":"ALL"
						},
						{
							"isDefault":false,
							"country":"US",
							"name":"US"
						},
						{
							"isDefault":false,
							"country":"Canada",
							"name":"Canada"
						}
					]
				},
				{
					"type":2,
					"list":[
					  {
							"isDefault":true,
							"country":"ALL",
							"name":"ALL"
						},
						{
							"isDefault":false,
							"country":"Australia",
							"name":"Australia"
						}
					]
				},
				{
					"type":3,
					"list":[
						{
							"isDefault":true,
							"country":"ALL",
							"name":"ALL"
						},
						{
							"isDefault":false,
							"country":"Brazil",
							"name":"Brazil"
						}
					]
				}
			],
			"lottery":[
			  {
					"type":0,
					"list": [
		        {
		          "country":"ALL",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/swedish-postcode-lottery.png",
		              "name": "Swedish Postcode Lottery",
		              "describe": "Sweden’s largest lottery！",
		              "format": "Format: -"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/sweden-lotto.png",
		              "name": "Sweden Lotto",
		              "describe": "Win your Dream Winnings!",
		              "format": "Format: 7/35"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/viking-lotto.png",
		              "name": "Viking Lotto",
		              "describe": "Huge jackpots, amazing winning odds, and seven excellent  prize divisions !",
		              "format": "Format: 6/48+1/8"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/finland-lotto.png",
		              "name": "Finland Lotto",
		              "describe": "With a guess range of only 1 to 40!",
		              "format": "Format: 7/40"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/superenalotto.png",
		              "name": "Superenalotto",
		              "describe": "Three opportunities to win the lotto's six prize categories each week！",
		              "format": "Format: 6/90"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/germany-lotto.png",
		              "name": "Germany Lotto",
		              "describe": "8 prize divisions and two weekly drawings",
		              "format": "Format: 6/49+1/9"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/el-gordo.png",
		              "name": "El Gordo",
		              "describe": "With a €5 million starting jackpot and eight further prize divisions .",
		              "format": "Format: 5/54+1/9"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/la-primitiva.png",
		              "name": "La Primitiva",
		              "describe": "Approximately $1.39 billion every year in all prize divisions.",
		              "format": "Format: 6/49+1/9"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/norway-lotto.png",
		              "name": "Norway Lotto",
		              "describe": "The biggest game in Norway!",
		              "format": "Format: 7/34"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/irish-lotto.png",
		              "name": "Irish Lotto",
		              "describe": "With the format 6 / 47, it offers you very good chances of winning",
		              "format": "Format: 6/47"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/bonoloto.png",
		              "name": "Bonoloto",
		              "describe": "Great winning odds and six lottery draws each week!",
		              "format": "Format: 6/49"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/poland-lotto.png",
		              "name": "Poland Lotto",
		              "describe": "Classic European play format featuring great jackpots !",
		              "format": "Format: 6/49"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/swiss-lotto.png",
		              "name": "Swiss Lotto",
		              "describe": "Small guess range and eight prize divisions!",
		              "format": "Format: 6/42+1/6"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/uk-national-lottery.png",
		              "name": "UK National Lottery",
		              "describe": "Amazing odds and six great prize divisions.",
		              "format": "Format: 6/59"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/austria-lotto.png",
		              "name": "Austria Lotto",
		              "describe": "With a 1 in 16 chance of winning and 9 attractive prize categories.",
		              "format": "Format: 6/45"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/belgium-lotto.png",
		              "name": "Belgium Lotto",
		              "describe": "1 in 8 million odds of winning the jackpot!",
		              "format": "Format: 6/45"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/poland-mini-lotto.png",
		              "name": "Poland Mini Lotto",
		              "describe": "Poland’s Mini Lotto game is easy to play!",
		              "format": "Format: 5/42"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions-hotpicks.png",
		              "name": "EuroMillions HotPicks",
		              "describe": "Bigger prizes for matching fewer EuroMillions numbers",
		              "format": "Format:  1-5/50"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/thunderball.png",
		              "name": "Thunderball",
		              "describe": "The top prize on Thunderball is not shared,4 draws a week!",
		              "format": "Format:  5/39 + 1/14"
		            }
		          ]
		        },
		        {
		          "country":"Sweden",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/swedish-postcode-lottery.png",
		              "name": "Swedish Postcode Lottery",
		              "describe": "Sweden’s largest lottery！",
		              "format": "Format: -"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/sweden-lotto.png",
		              "name": "Sweden Lotto",
		              "describe": "Win your Dream Winnings!",
		              "format": "Format: 7/35"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/viking-lotto.png",
		              "name": "Viking Lotto",
		              "describe": "Huge jackpots, amazing winning odds, and seven excellent  prize divisions !",
		              "format": "Format: 6/48+1/8"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            }
		          ]
		        },
		        {
		          "country":"Germany",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/germany-lotto.png",
		              "name": "Germany Lotto",
		              "describe": "8 prize divisions and two weekly drawings",
		              "format": "Format: 6/49+1/9"
		            }
		          ]
		        },
		        {
		          "country":"UK",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/uk-national-lottery.png",
		              "name": "UK National Lottery",
		              "describe": "Amazing odds and six great prize divisions.",
		              "format": "Format: 6/59"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions-hotpicks.png",
		              "name": "EuroMillions HotPicks",
		              "describe": "Bigger prizes for matching fewer EuroMillions numbers",
		              "format": "Format:  1-5/50"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/thunderball.png",
		              "name": "Thunderball",
		              "describe": "The top prize on Thunderball is not shared,4 draws a week!",
		              "format": "Format:  5/39 + 1/14"
		            }
		          ]
		        },
		        {
		          "country":"Finland",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/viking-lotto.png",
		              "name": "Viking Lotto",
		              "describe": "Huge jackpots, amazing winning odds, and seven excellent  prize divisions !",
		              "format": "Format: 6/48+1/8"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/finland-lotto.png",
		              "name": "Finland Lotto",
		              "describe": "With a guess range of only 1 to 40!",
		              "format": "Format: 7/40"
		            }
		          ]
		        },
		        {
		          "country":"Ireland",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/irish-lotto.png",
		              "name": "Irish Lotto",
		              "describe": "With the format 6 / 47, it offers you very good chances of winning",
		              "format": "Format: 6/47"
		            }
		          ]
		        },
		        {
		          "country":"Switzerland",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            }
		          ]
		        },
		        {
		          "country":"Spain",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/el-gordo.png",
		              "name": "El Gordo",
		              "describe": "With a €5 million starting jackpot and eight further prize divisions .",
		              "format": "Format: 5/54+1/9"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/la-primitiva.png",
		              "name": "La Primitiva",
		              "describe": "Approximately $1.39 billion every year in all prize divisions.",
		              "format": "Format: 6/49+1/9"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/bonoloto.png",
		              "name": "Bonoloto",
		              "describe": "Great winning odds and six lottery draws each week!",
		              "format": "Format: 6/49"
		            }
		          ]
		        },
		        {
		          "country":"Austria",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/austria-lotto.png",
		              "name": "Austria Lotto",
		              "describe": "With a 1 in 16 chance of winning and 9 attractive prize categories.",
		              "format": "Format: 6/45"
		            }
		          ]
		        },
		        {
		          "country":"Norway",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/viking-lotto.png",
		              "name": "Viking Lotto",
		              "describe": "Huge jackpots, amazing winning odds, and seven excellent  prize divisions !",
		              "format": "Format: 6/48+1/8"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/norway-lotto.png",
		              "name": "Norway Lotto",
		              "describe": "The biggest game in Norway!",
		              "format": "Format: 7/34"
		            }
		          ]
		        },
		        {
		          "country":"Poland",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/poland-lotto.png",
		              "name": "Poland Lotto",
		              "describe": "Classic European play format featuring great jackpots !",
		              "format": "Format: 6/49"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/poland-mini-lotto.png",
		              "name": "Poland Mini Lotto",
		              "describe": "Poland’s Mini Lotto game is easy to play!",
		              "format": "Format: 5/42"
		            }
		          ]
		        },
		        {
		          "country":"Belgium",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/belgium-lotto.png",
		              "name": "Belgium Lotto",
		              "describe": "1 in 8 million odds of winning the jackpot!",
		              "format": "Format: 6/45"
		            }
		          ]
		        },
		        {
		          "country":"Italy",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/superenalotto.png",
		              "name": "Superenalotto",
		              "describe": "Three opportunities to win the lotto's six prize categories each week！",
		              "format": "Format: 6/90"
		            }
		          ]
		        },
		        {
		          "country":"Denmark",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/viking-lotto.png",
		              "name": "Viking Lotto",
		              "describe": "Huge jackpots, amazing winning odds, and seven excellent  prize divisions !",
		              "format": "Format: 6/48+1/8"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            }
		          ]
		        },
		        {
		          "country":"Estonia",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/viking-lotto.png",
		              "name": "Viking Lotto",
		              "describe": "Huge jackpots, amazing winning odds, and seven excellent  prize divisions !",
		              "format": "Format: 6/48+1/8"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            }
		          ]
		        },
		        {
		          "country":"Iceland",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/viking-lotto.png",
		              "name": "Viking Lotto",
		              "describe": "Huge jackpots, amazing winning odds, and seven excellent  prize divisions !",
		              "format": "Format: 6/48+1/8"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            }
		          ]
		        },
		        {
		          "country":"Latvia",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/viking-lotto.png",
		              "name": "Viking Lotto",
		              "describe": "Huge jackpots, amazing winning odds, and seven excellent  prize divisions !",
		              "format": "Format: 6/48+1/8"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            }
		          ]
		        },
		        {
		          "country":"Lithuania",
		          "list": [
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/eurojackpot.png",
		              "name": "Eurojackpot",
		              "describe": "An impressive 12 prize categories and attractive winning odds",
		              "format": "Format: 5/50+2/10"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/viking-lotto.png",
		              "name": "Viking Lotto",
		              "describe": "Huge jackpots, amazing winning odds, and seven excellent  prize divisions !",
		              "format": "Format: 6/48+1/8"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/euromillions.png",
		              "name": "Euromillions",
		              "describe": "Jackpot ranges from €17 million to €190 million ！",
		              "format": "Format: 5/50+1/12"
		            }
		          ]
		        }
		      ]
				},
			  {
					"type":1,
					"list":[
						{
							"country":"ALL",
							"list":[
								{
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/powerball.png",
		              "name": "Powerball",
		              "describe": "Top prize starts at $40 million！",
		              "format": "Format: 5/69+1/26"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/mega-millions.png",
		              "name": "Mega Millions",
		              "describe": "A guaranteed starting jackpot of $40 million and a second division prize guaranteed at $1 million",
		              "format": "Format: 5/70+1/25"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/cash4life.png",
		              "name": "Cash4life",
		              "describe": "Match all five white balls and the Cash Ball to win the $1,000 a Day !",
		              "format": "Format: 5/60 +  1/4"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/new-york-lottery.png",
		              "name": "New York Lottery",
		              "describe": "The largest lottery in New York.",
		              "format": "Format: 6/59"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/california-superlotto.png",
		              "name": "California Superlotto",
		              "describe": "Create jackpots larger than most US state lotteries!",
		              "format": "Format: 5/47 +  1/27"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/florida-lottery.png",
		              "name": "Florida Lottery",
		              "describe": "With  $2-$3 million starting jackpot, no rollover limit, and no jackpot cap!",
		              "format": "Format: 6/53"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/hoosier-lottery.png",
		              "name": "Hoosier Lottery",
		              "describe": "The best winning odds in the US with 1 out of 6 odds of taking home a prize.",
		              "format": "Format: 6/46"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/illinois-lottery.png",
		              "name": "Illinois Lottery",
		              "describe": "Jackpot starts at $2 million and drawings three times a week.",
		              "format": "Format: 6/52"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/canada-lotto649.png",
		              "name": "Canada Lotto649",
		              "describe": "Everyone's favourite national lottery, known for its record jackpots and easy-to-play format.",
		              "format": "Format: 6/49"
		            }
							]
		        },
		        {
							"country":"US",
							"list":[
								{
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/powerball.png",
		              "name": "Powerball",
		              "describe": "Top prize starts at $40 million！",
		              "format": "Format: 5/69+1/26"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/mega-millions.png",
		              "name": "Mega Millions",
		              "describe": "A guaranteed starting jackpot of $40 million and a second division prize guaranteed at $1 million",
		              "format": "Format: 5/70+1/25"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/new-york-lottery.png",
		              "name": "New York Lottery",
		              "describe": "The largest lottery in New York.",
		              "format": "Format: 6/59"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/california-superlotto.png",
		              "name": "California Superlotto",
		              "describe": "Create jackpots larger than most US state lotteries!",
		              "format": "Format: 5/47 +  1/27"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/florida-lottery.png",
		              "name": "Florida Lottery",
		              "describe": "With  $2-$3 million starting jackpot, no rollover limit, and no jackpot cap!",
		              "format": "Format: 6/53"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/hoosier-lottery.png",
		              "name": "Hoosier Lottery",
		              "describe": "The best winning odds in the US with 1 out of 6 odds of taking home a prize.",
		              "format": "Format: 6/46"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/illinois-lottery.png",
		              "name": "Illinois Lottery",
		              "describe": "Jackpot starts at $2 million and drawings three times a week.",
		              "format": "Format: 6/52"
		            }
							]
		        },
		        {
							"country":"Canada",
							"list":[
								{
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/canada-lotto649.png",
		              "name": "Canada Lotto649",
		              "describe": "Everyone's favourite national lottery, known for its record jackpots and easy-to-play format.",
		              "format": "Format: 6/49"
		            }
							]
		        }
					]
				},
			  {
					"type":2,
					"list":[
						{
							"country":"ALL",
							"list":[
								{
		              "logo": "https://d2ss3jaw7yg7ks.cloudfront.net/assets/img/lotto/play-center/saturday-tattslotto.png",
		              "name": "Saturday Tattslotto",
		              "describe": "The jackpots start at AU$ 4 million, and there are no taxes and no cap on jackpots or rollovers.",
		              "format": "Format: 6/45"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/australia-powerball.png",
		              "name": "Australia Powerball",
		              "describe": "Attracts Aussie Lotto fans with great jackpots, attractive winning odds, and tax-free prizes.",
		              "format": "Format: 7/35 + 1/20"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/australia-monday-wednesday.png",
		              "name": "Australia Monday Wednesday",
		              "describe": "There is a guaranteed $1 Million prize available for up to 4 winners!",
		              "format": "Format: 6/45"
		            }
							]
		        },
		        {
							"country":"Australia",
							"list":[
								{
		              "logo": "https://d2ss3jaw7yg7ks.cloudfront.net/assets/img/lotto/play-center/saturday-tattslotto.png",
		              "name": "Saturday Tattslotto",
		              "describe": "The jackpots start at AU$ 4 million, and there are no taxes and no cap on jackpots or rollovers.",
		              "format": "Format: 6/45"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/australia-powerball.png",
		              "name": "Australia Powerball",
		              "describe": "Attracts Aussie Lotto fans with great jackpots, attractive winning odds, and tax-free prizes.",
		              "format": "Format: 7/35 + 1/20"
		            },
		            {
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/australia-monday-wednesday.png",
		              "name": "Australia Monday Wednesday",
		              "describe": "There is a guaranteed $1 Million prize available for up to 4 winners!",
		              "format": "Format: 6/45"
		            }
							]
		        }
					]
				},
				{
					"type":3,
					"list":[
						{
							"country":"ALL",
							"list":[
								{
									"logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/mega-sena.png",
				          "name": "Mega Sena",
				          "describe": "Unique two-drum lottery structure and three substantial prizes each draw.",
				          "format": "Format: 6/49"
								}
							]
		        },
		        {
							"country":"Brazil",
							"list":[
								{
		              "logo": "https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lotto/play-center/mega-sena.png",
		              "name": "Mega Sena",
		              "describe": "Unique two-drum lottery structure and three substantial prizes each draw.",
		              "format": "Format: 6/49"
		            }
							]
		        }
					]
				}
			]
		}
	})
}

export const getPlayState = () => {
	return Mock.mock({
		"status":0,
	  "code":0,
		"data":[
			{
				"name":"Europe",
	      "nation":[
	        {
	          "name":"Sweden",
	          "expand":true,
	          "lotto":["Swedish Postcode Lottery","Sweden Lotto","Eurojackpot","Viking Lotto","Euromillions"]
	        },
	        {
	          "name":"Germany",
	          "expand":false,
	          "lotto":["Eurojackpot","Sweden Lotto","Euromillions","Germany Lotto"]
	        },
	        {
	          "name":"UK",
	          "expand":false,
	          "lotto":["Eurojackpot","Euromillions","Euromillions","UK National Lottery","EuroMillions HotPicks","Thunderball"]
	        },
	        {
	          "name":"Finland",
	          "expand":false,
	          "lotto":["Eurojackpot","Viking Lotto","Euromillions","Finland Lotto"]
	        },
	        {
	          "name":"Ireland",
	          "expand":false,
	          "lotto":["Eurojackpot","Euromillions","Irish Lotto"]
	        },
	        {
	          "name":"Switzerland",
	          "expand":false,
	          "lotto":["Eurojackpot","Euromillions"]
	        },
	        {
	          "name":"Spain",
	          "expand":false,
	          "lotto":["Eurojackpot","Euromillions","El Gordo","La Primitiva","Bonoloto"]
	        },
	        {
	          "name":"Austria",
	          "expand":false,
	          "lotto":["Eurojackpot","Euromillions","Austria Lotto"]
	        },
	        {
	          "name":"Norway",
	          "expand":false,
	          "lotto":["Eurojackpot","Viking Lotto","Euromillions","Norway Lotto"]
	        },
	        {
	          "name":"Poland",
	          "expand":false,
	          "lotto":["Eurojackpot","Euromillions","Poland Lotto","Poland Mini Lotto"]
	        },
	        {
	          "name":"Belgium",
	          "expand":false,
	          "lotto":["Eurojackpot","Euromillions","Belgium Lotto"]
	        },
	        {
	          "name":"Italy",
	          "expand":false,
	          "lotto":["Eurojackpot","Euromillions","Superenalotto"]
	        },
	        {
	          "name":"Denmark",
	          "expand":false,
	          "lotto":["Eurojackpot","Viking Lotto","Euromillions"]
	        },
	        {
	          "name":"Estonia",
	          "expand":false,
	          "lotto":["Eurojackpot","Viking Lotto","Euromillions"]
	        },
	        {
	          "name":"Iceland",
	          "expand":false,
	          "lotto":["Eurojackpot","Viking Lotto","Euromillions"]
	        },
	        {
	          "name":"Latvia",
	          "expand":false,
	          "lotto":["Eurojackpot","Viking Lotto","Euromillions"]
	        },
	        {
	          "name":"Lithuania",
	          "expand":false,
	          "lotto":["Eurojackpot","Viking Lotto","Euromillions"]
	        }
	      ]
			},
			{
		    "name":"North America",
	      "nation":[
	        {
	          "name":"U.S.",
	          "expand":false,
	          "lotto":["Powerball","Mega Millions","New York Lottery","California Superlotto","Florida Lottery","Hoosier Lottery","Illinois Lottery"]
	        },
	        {
	          "name":"Canada",
	          "expand":false,
	          "lotto":["Canada Lotto649"]
	        }
	      ]
			},
			{
		    "name":"Oceania",
	      "nation":[
	        {
	          "name":"Australia",
	          "expand":false,
	          "lotto":["Saturday Tattslotto","Australia Powerball","Australia Monday Wednesday"]
	        }
	      ]
			},
			{
		    "name":"South America",
	      "nation":[
	        {
	          "name":"Brazil",
	          "expand":false,
	          "lotto":["Mega Sena"]
	        }
	      ]
			}
		]
	})
}

export const getLottoMenu = () => {
	return Mock.mock({
		"status":0,
    "code":0,
		"data":[
			{
	      "name":"Popular",
	      "type":0
	    },
	    {
	      "name":"Biggest jackpots",
	      "type":1
	    },
	    {
	      "name":"A-Z",
	      "type":2
	    },
	    {
	      "name":"Closing soon",
	      "type":3
	    },
	    {
	      "name":"Must lotto",
	      "type":4
	    }
	  ]
	})
}

export const getLottoInfo = () => {
	return Mock.mock({
		"status":0,
    "code":0,
		"data":[
	    {
	      "type": 0,
	      "list": [
	        {
	          "name": "Euromillions",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "17 Million",
	          "unit_jackpot": "17,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 12:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/euromillions.png",
	          "boost": false,
	          "unit_price": "3.5"
	        },
	        {
	          "name": "Powerball",
	          "user_currency": "EUR",
	          "symbollocation":"after",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 16:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/usa-powerball.png",
	          "boost": true,
	          "unit_price": "3.5"
	        },
	        {
	          "name": "Eurojackpot",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "boost_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 21:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/eurojackpot.png",
	          "boost": false,
	          "unit_price": "2.5"
	        },
	        {
	          "name": "Mega Millions",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 21:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/mega-millions.png",
	          "boost": true,
	          "unit_price": "5.0"
	        },
	        {
	          "name": "Finland Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 16:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/finland-lotto.png",
	          "boost": true,
	          "unit_price": "3.0"
	        },
	        {
	          "name": "Swiss Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 16:20:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/swiss-lotto.png",
	          "boost": false,
	          "unit_price": "2.5"
	        },
	        {
	          "name": "SuperEnaLotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-24 10:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/super-ena-lotto.png",
	          "boost": false,
	          "unit_price": "1.0"
	        },
	        {
	          "name": "California SuperLotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 10:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/california-super-lotto.png",
	          "boost": false,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "New York Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 12:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/new-york-lotto.png",
	          "boost": false,
	          "unit_price": "1.5"
	        },
	        {
	          "name": "Lotto2020",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-25 12:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/year-lottery.png",
	          "boost": true,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "Bitcoin jackpot",
	          "user_currency": "Ƀ",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-25 08:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/bitcoin-jackpot.png",
	          "boost": false,
	          "unit_price": "3.0"
	        },
	        {
	          "name": "El Gordo",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 08:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/el-gordo.png",
	          "boost": false,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "Viking Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 22:10:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/sweden-viking-lotto.png",
	          "boost": false,
	          "unit_price": "0.8"
	        }
	      ]
	    },
	    {
	      "type": 1,
	      "list": [
	        // {
	        //   "name": "Lotto2020",
	        //   "user_currency": "EUR",
	        //   "symbollocation":"before",
	        //   "jackpot": "26.7 Million",
	        //   "unit_jackpot": "26 700,000",
	        //   "boost_jackpot": "355,4 Million",
	        //   "unit_boost_jackpot": "355,400,000",
	        //   "draw_day": "2020-04-21",
	        //   "draw_date": "2020-12-21 23:00:00",
	        //   "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/year-lottery.png",
	        //   "boost": true,
	        //   "unit_price": "2.0"
	        // },
	        // {
	        //   "name": "Powerball",
	        //   "user_currency": "EUR",
	        //   "symbollocation":"after",
	        //   "jackpot": "26.7 Million",
	        //   "unit_jackpot": "26 700,000",
	        //   "boost_jackpot": "355,4 Million",
	        //   "unit_boost_jackpot": "355,400,000",
	        //   "draw_day": "2020-04-21",
	        //   "draw_date": "2020-12-21 20:30:00",
	        //   "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/usa-powerball.png",
	        //   "boost": true,
	        //   "unit_price": "3.5"
	        // },
	        {
	          "name": "Mega Millions",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 21:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/mega-millions.png",
	          "boost": true,
	          "unit_price": "5.0"
	        },
	        {
	          "name": "Eurojackpot",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-27 10:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/eurojackpot.png",
	          "boost": false,
	          "unit_price": "2.5"
	        },
	        {
	          "name": "Euromillions",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-25 09:20:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/euromillions.png",
	          "boost": false,
	          "unit_price": "3.5"
	        },
	        {
	          "name": "SuperEnaLotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-23 09:20:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/super-ena-lotto.png",
	          "boost": false,
	          "unit_price": "1.0"
	        },
	        {
	          "name": "Swiss Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-27 10:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/swiss-lotto.png",
	          "boost": false,
	          "unit_price": "2.5"
	        },
	        // },
	        // {
	        //   "name": "Finland Lotto",
	        //   "user_currency": "EUR",
	        //   "symbollocation":"before",
	        //   "jackpot": "26.7 Million",
	        //   "unit_jackpot": "26 700,000",
	        //   "boost_jackpot": "355,4 Million",
	        //   "unit_boost_jackpot": "355,400,000",
	        //   "draw_day": "2020-04-21",
	        //   "draw_date": "2020-12-27 12:30:00",
	        //   "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/finland-lotto.png",
	        //   "boost": true,
	        //   "unit_price": "3.0"
	        // },
	        // {
	        //   "name": "El Gordo",
	        //   "user_currency": "EUR",
	        //   "symbollocation":"before",
	        //   "jackpot": "26.7 Million",
	        //   "unit_jackpot": "61,000,000",
	        //   "draw_day": "2020-04-21",
	        //   "draw_date": "2020-12-28 10:30:00",
	        //   "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/el-gordo.png",
	        //   "boost": false,
	        //   "unit_price": "2.0"
	        // },
	        // {
	        //   "name": "Bitcoin jackpot",
	        //   "user_currency": "Ƀ",
	        //   "symbollocation":"before",
	        //   "jackpot": "26.7 Million",
	        //   "unit_jackpot": "61,000,000",
	        //   "draw_day": "2020-04-21",
	        //   "draw_date": "2020-12-30 22:30:00",
	        //   "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/bitcoin-jackpot.png",
	        //   "boost": false,
	        //   "unit_price": "3.0"
	        // },
	        // {
	        //   "name": "Viking Lotto",
	        //   "user_currency": "EUR",
	        //   "symbollocation":"before",
	        //   "jackpot": "26.7 Million",
	        //   "unit_jackpot": "61,000,000",
	        //   "draw_day": "2020-04-21",
	        //   "draw_date": "2020-12-30 12:45:00",
	        //   "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/sweden-viking-lotto.png",
	        //   "boost": false,
	        //   "unit_price": "0.8"
	        // },
	        {
	          "name": "California SuperLotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 12:45:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/california-super-lotto.png",
	          "boost": false,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "New York Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 17:45:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/new-york-lotto.png",
	          "boost": false,
	          "unit_price": "1.5"
	        }
	      ]
	    },
	    {
	      "type": 2,
	      "list": [
	        {
	          "name": "Bitcoin jackpot",
	          "user_currency": "Ƀ",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 15:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/bitcoin-jackpot.png",
	          "boost": false,
	          "unit_price": "3.0"
	        },
	        {
	          "name": "California SuperLotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 16:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/california-super-lotto.png",
	          "boost": false,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "El Gordo",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 16:40:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/el-gordo.png",
	          "boost": false,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "Eurojackpot",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 18:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/eurojackpot.png",
	          "boost": false,
	          "unit_price": "2.5"
	        },
	        {
	          "name": "Euromillions",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 23:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/euromillions.png",
	          "boost": false,
	          "unit_price": "3.5"
	        },
	        {
	          "name": "Finland Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 20:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/finland-lotto.png",
	          "boost": true,
	          "unit_price": "3.0"
	        },
	        {
	          "name": "Lotto2020",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "boost_jackpot": "355.4 Million",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-23 12:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/year-lottery.png",
	          "boost": true,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "Mega Millions",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-23 14:45:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/mega-millions.png",
	          "boost": true,
	          "unit_price": "5.0"
	        },
	        {
	          "name": "New York Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-24 17:45:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/new-york-lotto.png",
	          "boost": false,
	          "unit_price": "1.5"
	        },
	        {
	          "name": "Powerball",
	          "user_currency": "EUR",
	          "symbollocation":"after",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-24 18:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/usa-powerball.png",
	          "boost": true,
	          "unit_price": "3.5"
	        },
	        {
	          "name": "SuperEnaLotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-24 20:10:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/super-ena-lotto.png",
	          "boost": false,
	          "unit_price": "1.0"
	        },
	        {
	          "name": "Swiss Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-25 10:10:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/swiss-lotto.png",
	          "boost": false,
	          "unit_price": "2.5"
	        },
	        {
	          "name": "Viking Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-26 13:15:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/sweden-viking-lotto.png",
	          "boost": false,
	          "unit_price": "0.8"
	        }
	      ]
	    },
	    {
	      "type": 3,
	      "list": [
	        {
	          "name": "Finland Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_date": "2020-12-21 13:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/finland-lotto.png",
	          "boost": true,
	          "unit_price": "3.0"
	        },
	        {
	          "name": "Euromillions",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 14:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/euromillions.png",
	          "boost": false,
	          "unit_price": "3.5"
	        },
	        {
	          "name": "SuperEnaLotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 14:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/super-ena-lotto.png",
	          "boost": false,
	          "unit_price": "1.0"
	        },
	        {
	          "name": "Mega Millions",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 17:10:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/mega-millions.png",
	          "boost": true,
	          "unit_price": "5.0"
	        },
	        {
	          "name": "New York Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-21 20:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/new-york-lotto.png",
	          "boost": false,
	          "unit_price": "1.5"
	        },
	        {
	          "name": "Powerball",
	          "user_currency": "EUR",
	          "symbollocation":"after",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 07:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/usa-powerball.png",
	          "boost": true,
	          "unit_price": "3.5"
	        },
	        {
	          "name": "California SuperLotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 08:10:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/california-super-lotto.png",
	          "boost": false,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "Lotto2020",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 11:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/year-lottery.png",
	          "boost": true,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "Eurojackpot",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 14:20:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/eurojackpot.png",
	          "boost": false,
	          "unit_price": "2.5"
	        },
	        {
	          "name": "Bitcoin jackpot",
	          "user_currency": "Ƀ",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 17:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/bitcoin-jackpot.png",
	          "boost": false,
	          "unit_price": "3.0"
	        },
	        {
	          "name": "El Gordo",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 18:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/el-gordo.png",
	          "boost": false,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "Viking Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-23 12:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/sweden-viking-lotto.png",
	          "boost": false,
	          "unit_price": "0.8"
	        },
	        {
	          "name": "Swiss Lotto",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-24 12:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/md/swiss-lotto.png",
	          "boost": false,
	          "unit_price": "2.5"
	        }
	      ]
	    },
	    {
	      "type": 4,
	      "list": [
	        {
	          "name": "California SuperLotto",
	          "desc": "California Dreaming!",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-22 12:00:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/lg/california-super-lotto.png",
	          "mustbg":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lottobg/norway-lotto.jpg",
	          "boost": false,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "New York Lotto",
	          "desc": "Great Value And Big Wins!",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-23 15:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/lg/germany-lotto.png",
	          "mustbg":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lottobg/irish-lotto.jpg",
	          "boost": false,
	          "unit_price": "1.5"
	        },
	        {
	          "name": "Finland Lotto",
	          "desc": "Finland's €1 Lotto!",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-23 17:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/lg/finland-lotto.png",
	          "mustbg":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lottobg/germany-lotto.jpg",
	          "boost": true,
	          "unit_price": "3.0"
	        },
	        {
	          "name": "Irish Lotto",
	          "desc": "Ireland's Top Jackpot",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-24 12:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/lg/irish-lotto.png",
	          "mustbg":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lottobg/mega-sena.jpg",
	          "boost": true,
	          "unit_price": "2.0"
	        },
	        {
	          "name": "Euromillions",
	          "desc": "Ireland's Top Jackpot",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-27 10:10:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/lg/euromillions.png",
	          "mustbg":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lottobg/mega-sena.jpg",
	          "boost": false,
	          "unit_price": "3.5"
	        },
	        {
	          "name": "Powerball",
	          "desc": "Ireland's Top Jackpot",
	          "user_currency": "EUR",
	          "symbollocation":"after",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "26 700,000",
	          "boost_jackpot": "355,4 Million",
	          "unit_boost_jackpot": "355,400,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-28 15:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/lg/usa-powerball.png",
	          "mustbg":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lottobg/mega-sena.jpg",
	          "boost": true,
	          "unit_price": "3.5"
	        },
	        {
	          "name": "Eurojackpot",
	          "desc": "Ireland's Top Jackpot",
	          "user_currency": "EUR",
	          "symbollocation":"before",
	          "jackpot": "26.7 Million",
	          "unit_jackpot": "61,000,000",
	          "draw_day": "2020-04-21",
	          "draw_date": "2020-12-29 10:30:00",
	          "logo": "https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/lotto/lg/eurojackpot.png",
	          "mustbg":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/lottobg/mega-sena.jpg",
	          "boost": false,
	          "unit_price": "2.5"
	        }
	      ]
	    }
	  ]
	})
}

export const getResultInfo = () => {
	return Mock.mock({
		"status":0,
	  "code":0,
		"data":{
      "date": "April 2020",
			"list": [
		    {
		      "date":"Tuesday, 28 April",
		      "time":"28",
		      "draw_date":"2020-04-28",
		      "user_currency": "EUR",
		      "name":"Mega Millions",
		      "ball_number":[13,19,53,54,63],
		      "extra_number":[],
		      "bonus_number":[17],
		      "jackpot": "78.8 Million"
		    },
		    {
		      "date":"Tuesday, 28 April",
		      "time":"25",
		      "draw_date":"2020-04-28",
		      "user_currency": "EUR",
		      "name":"Powerball",
		      "ball_number":[1,3,21,47,57],
		      "extra_number":[],
		      "bonus_number":[18],
		      "jackpot": "128.0 Million"
		    },
		    {
		      "date":"Tuesday, 28 April",
		      "time":"28",
		      "draw_date":"2020-04-28",
		      "user_currency": "EUR",
		      "name":"South Africa Powerball",
		      "ball_number":[6,12,32,36,45],
		      "extra_number":[],
		      "bonus_number":[4],
		      "jackpot": "178.5 Million"
		    },
		    {
		      "date":"Tuesday, 28 April",
		      "time":"28",
		      "draw_date":"2020-04-28",
		      "user_currency": "EUR",
		      "name":"South Africa Daily Lotto",
		      "ball_number":[1,3,17,21,22],
		      "extra_number":[],
		      "bonus_number":[],
		      "jackpot": "178.5 Million"
		    },
		    {
		      "date":"Tuesday, 28 April",
		      "time":"28",
		      "draw_date":"2020-04-28",
		      "user_currency": "EUR",
		      "name":"Cash4Life",
		      "ball_number":[18,21,36,41,48],
		      "extra_number":[],
		      "bonus_number":[2],
		      "jackpot": "77.8 Million"
		    },
		    {
		      "date":"Tuesday, 28 April",
		      "time":"28",
		      "draw_date":"2020-04-28",
		      "user_currency": "EUR",
		      "name":"Poland Lotto",
		      "ball_number":[9,13,23,24,3,44],
		      "extra_number":[],
		      "bonus_number":[],
		      "jackpot": "94.8 Million"
		    }
		  ]
		}
	})
}

export const getResultState = () => {
	return Mock.mock({
		"status":0,
	  "code":0,
		"data":[
			{
				"name":"Europe",
	      "nation":["Austria Lotto","Belgium Lotto","Belgium Joker+","Bulgaria Toto2 6/49","Bulgaria Toto2 6/42","Bulgaria Toto2 5/50 Zodiac","Croatia Loto 7","Croatia Loto 6/45","Eurojackpot","Euromillions","Finland Lotto","Germany Lotto","Germany Spiel 77","Germany Super 6","Hungary Otoslotto","Hungary Hatoslotto","Hungary Joker","Irish Lotto plus 1","Irish Lotto plus 2","Irish Lotto","SuperEnaLotto","Latvia Latloto 5/35","Norway Lotto","Poland Lotto","Portugal Totoloto","Mini Lotto","Romania Loto 6/49","Romania Loto 5/40","Romania joker","Russia Gosloto 6/36","Slovenia Loto","Slovenia Loto Plus","Slovenia Lotko","Bonoloto","El Gordo","Bitcoin jackpot","Lotto2020","La Primitiva","Christmas Lottery","Gold Lottery","Swiss Lotto","Viking Lotto","Sweden Lotto","Turkey Sayisal Loto 6/49","Turkey super lotto","Turkey sans topu","UK National Lottery","Ukraine super lotto","Ukraine Lotto Maxima","Ukraine Megalot"]
			},
	    {
	      "name":"USA",
	      "nation":["Hoosier Lotto","Cash4Life","Illinois Lotto","New York Lotto","Powerball","California SuperLotto","Florida Lotto","Mega Millions"]
	    },
	    {
	      "name":"Australia",
	      "nation":["Australia Powerball","Saturday Lotto"]
	    },
	    {
	      "name":"South America",
	      "nation":["Brazil Dia de Sorte","Brazil Quina","Mega-Sena","Colombia Baloto Revancha","Colombia Baloto","Mexico Melate Retro","Mexico Melate"]
	    },
	    {
	      "name":"North America",
	      "nation":["Canada Lotto Max","Canada Lotto"]
	    },
	    {
	      "name":"Africa",
	      "nation":["South Africa Daily Lotto"]
	    },
	    {
	      "name":"Asia",
	      "nation":["Hong Kong Mark Six","Israel Lotto","Japan loto 6","Japan loto 7"]
	    }
		]
	})
}

export const getResultDetail = (option) => {
	let {name,date} = JSON.parse(option.body);
	if ( ('mega-millions' == name) && (date == '2020-04-03') ) {
		return {
			"status":0,
		  "code":0,
		  "data":{
		  	"name":"Mega Millions",
			  "draw_date": "03 April 2020",
				"number": {
			    "ball_number":[24,38,44,57,58],
			    "extra_number":[],
			    "bonus_number":[17]
			  },
			  "draw_info": [
			    {
			      "match":"5+1",
			      "odds":"1:302 575 350",
			      "prize":"No winner"
			    },
			    {
			      "match":"5",
			      "odds":"1:12 607 306",
			      "prize":"€922 253.99"
			    },
			    {
			      "match":"4+1",
			      "odds":"1:931 001",
			      "prize":"€9 222.54"
			    },
			    {
			      "match":"4",
			      "odds":"1:38 792",
			      "prize":"€461.13"
			    },
			    {
			      "match":"3+1",
			      "odds":"1:14 547",
			      "prize":"€184.45"
			    },
			    {
			      "match":"3",
			      "odds":"1:606",
			      "prize":"€9.22"
			    },
			    {
			      "match":"2+1",
			      "odds":"1:693",
			      "prize":"€9.22"
			    },
			    {
			      "match":"1+1",
			      "odds":"1:89",
			      "prize":"€3.69"
			    },
			    {
			      "match":"0+1",
			      "odds":"1:37",
			      "prize":"€1.84"
			    }
			  ]
		  }
		}
	}
	if ( ('mega-millions' == name) && (date == '2020-04-28') ) {
		return {
			"status":0,
		  "code":0,
		  "data":{
		  	"name":"Mega Millions",
			  "draw_date": "28 April 2020",
			  "number": {
			    "ball_number":[13,19,53,54,63],
			    "extra_number":[],
			    "bonus_number":[17]
			  },
			  "draw_info": [
			    {
			      "match":"5+1",
			      "odds":"1:302 575 350",
			      "prize":"No winner"
			    },
			    {
			      "match":"5",
			      "odds":"1:12 607 306",
			      "prize":"€922 253.99"
			    },
			    {
			      "match":"4+1",
			      "odds":"1:931 001",
			      "prize":"€9 222.54"
			    },
			    {
			      "match":"4",
			      "odds":"1:38 792",
			      "prize":"€461.13"
			    },
			    {
			      "match":"3+1",
			      "odds":"1:14 547",
			      "prize":"€184.45"
			    },
			    {
			      "match":"3",
			      "odds":"1:606",
			      "prize":"€9.22"
			    },
			    {
			      "match":"2+1",
			      "odds":"1:693",
			      "prize":"€9.22"
			    },
			    {
			      "match":"1+1",
			      "odds":"1:89",
			      "prize":"€3.69"
			    },
			    {
			      "match":"0+1",
			      "odds":"1:37",
			      "prize":"€1.84"
			    }
			  ]
		  }
		}
	}
}

export const getResultDraw = (option) => {
	let name = option.url.replace("/api/getResultDraw/","");
  if ('powerball' == name) {
		return {
			"status":0,
		  "code":0,
		  "data":{
		  	"name":"Powerball",
			  "date": "April 2020",
				"list": [
			    {
			      "time":"25",
			      "date":"2020-04-25",
			      "ball_number":[1,3,21,47,57],
			      "extra_number":[],
			      "bonus_number":[18]
			    },
			    {
			      "time":"22",
			      "date":"2020-04-22",
			      "ball_number":[1,33,35,40,69],
			      "extra_number":[],
			      "bonus_number":[24]
			    },
			    {
			      "time":"18",
			      "date":"2020-04-18",
			      "ball_number":[4,44,46,56,63],
			      "extra_number":[],
			      "bonus_number":[19]
			    },
			    {
			      "time":"15",
			      "date":"2020-04-15",
			      "ball_number":[10,12,33,36,41],
			      "extra_number":[],
			      "bonus_number":[2]
			    },
			    {
			      "time":"11",
			      "date":"2020-04-11",
			      "ball_number":[22,29,30,42,47],
			      "extra_number":[],
			      "bonus_number":[17]
			    },
			    {
			      "time":"8",
			      "date":"2020-04-08",
			      "ball_number":[2,37,39,48,54],
			      "extra_number":[],
			      "bonus_number":[5]
			    },
			    {
			      "time":"4",
			      "date":"2020-04-04",
			      "ball_number":[8,31,39,40,43],
			      "extra_number":[],
			      "bonus_number":[4]
			    },
			    {
			      "time":"1",
			      "date":"2020-04-01",
			      "ball_number":[33,35,45,48,60],
			      "extra_number":[],
			      "bonus_number":[16]
			    }
				]
		  }
		}
  }
  if ('mega-millions' == name) {
		return {
			"status":0,
		  "code":0,
		  "data":{
		  	"name":"Mega Millions",
			  "date": "April 2020",
				"list": [
			    {
			      "time":"28",
			      "date":"2020-04-28",
			      "ball_number":[13,19,53,54,63],
			      "extra_number":[],
			      "bonus_number":[17]
			    },
			    {
			      "time":"24",
			      "date":"2020-04-24",
			      "ball_number":[1,27,32,60,67],
			      "extra_number":[],
			      "bonus_number":[18]
			    },
			    {
			      "time":"21",
			      "date":"2020-04-21",
			      "ball_number":[13,15,24,67,70],
			      "extra_number":[],
			      "bonus_number":[17]
			    },
			    {
			      "time":"17",
			      "date":"2020-04-17",
			      "ball_number":[13,35,39,46,55],
			      "extra_number":[],
			      "bonus_number":[14]
			    },
			    {
			      "time":"14",
			      "date":"2020-04-14",
			      "ball_number":[29,47,65,69,70],
			      "extra_number":[],
			      "bonus_number":[7]
			    },
			    {
			      "time":"10",
			      "date":"2020-04-10",
			      "ball_number":[2,11,21,57,60],
			      "extra_number":[],
			      "bonus_number":[13]
			    },
			    {
			      "time":"7",
			      "date":"2020-04-07",
			      "ball_number":[25,33,43,51,68],
			      "extra_number":[],
			      "bonus_number":[20]
			    },
			    {
			      "time":"3",
			      "date":"2020-04-03",
			      "ball_number":[24,38,44,57,58],
			      "extra_number":[],
			      "bonus_number":[17]
			    }
				]
		  }
		}
  }
}

export const getMethodList = () => {
	return {
		"status":0,
	  "code":0,
		"data":[
      {
      	'type':'account',
        'icon':'card-multilottoAccount',
        'name':'Multilotto Account',
        'description':'',
        'num':1000,
      },
      {
      	'type':'creditCard',
        'icon':'card-creditCard',
        'name':'Credit card',
        'description':'Most users choose this',
      },
      {
      	'type':'trustly',
        'icon':'card-trustly',
        'name':'',
        'description':'Instant Banking',
      },
      {
      	'type':'sofort',
        'icon':'card-sofort',
        'name':'',
        'description':'A popular payment method',
      },
      {
      	'type':'freeTicket',
        'icon':'card-freeTicket',
        'name':'',
        'description':'A popular payment method',
      },
      {
      	'type':'visa',
        'icon':'card-visa',
        'name':'',
        'description':'A popular payment method',
      },
      {
      	'type':'american-express',
        'icon':'card-american-express',
        'name':'',
        'description':'A popular payment method',
      }
    ]
	}
}

export const getAccountInfo = (option) => {
	let { realMoney } = JSON.parse(option.body);
	return {
		"status": 0,
	  "code": 0,
	  "data": {
	  	"realMoney": Number(localRead('realMoney')) + Number(realMoney),
		  "lottoMoney": localRead('lottoMoney'),
		  "casinoMoney": localRead('casinoMoney')
	  }
	}
}

export const getQuickInfo = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
	  "data": {
	  	"currency": "EUR",
      "currencysymbol": "€",
      "currencysymbollocation": "before",
      "cutoffseconds": 5,
      "defaultprice": "0.50",
      "description": "Every 5 minutes",
      "draws": [1,5,10],
      "gametypeid": 10001,
      "isplayable": 1,
      "logo": "/images/quick/logo.png",
      "minprice": "0.50",
      "name": "Lotto 5/11",
      "numbermax": 11,
      "numbermin": 1,
      "playtype": [
        {
          "description": "Match 3 balls to get 10x bonus",
          "id": 1,
          "name": "pick 3",
          "numbers": 3
        },
        {
          "description": "Match 4 balls to get 20x bonus",
          "id": 2,
          "name": "pick 4",
          "numbers": 4
        },
        {
          "description": "Match 5 balls to get 30x bonus",
          "id": 3,
          "name": "pick 5",
          "numbers": 5
        }
      ],
      "slug": "lotto5-11"
	  }
	})
}

export const getNoticeList = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
	  "data": {
	  	"list": ['Lotto5/11 is Keno style game with 5 minutes each draw, pick up to 3/4/5 numbers out of 11 numbers.','If selected number matches with the result set, customer is liable to get  Stake * Odds .','Lotto5/11 is Keno style game with 5 minutes each draw, pick up to 3/4/5 numbers out of 11 numbers. If selected number matches with the result set, customer is liable to get  Stake * Odds']
	  }
	})
}

export const getWinList = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
	  "data": {
	  	"list": [
		  	{
		  		"id":'1001',
		  		"title":'f*** r** has recently won',
		  		"amount":'€2 900.00'
		  	},
		  	{
		  		"id":'1002',
		  		"title":'J*** C** has recently won',
		  		"amount":'€747.60'
		  	},
		  	{
		  		"id":'1003',
		  		"title":'J*** C** has recently won',
		  		"amount":'€110.00'
		  	},
		  	{
		  		"id":'1004',
		  		"title":'a*** f** has recently won',
		  		"amount":'€900.00'
		  	},
		  	{
		  		"id":'1005',
		  		"title":'a*** f** has recently won',
		  		"amount":'€100.00'
		  	},
		  	{
		  		"id":'1006',
		  		"title":'f*** r** has recently won',
		  		"amount":'€700.00'
		  	}
	  	]
	  }
	})
}

export const getResultList = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
	  "data": {
	  	"list": [
		  	{
		  		"drawid": 1014561,
					"draw_time": "2020/10/25 10:50",
					"gametypeid": 10001,
					"current_draw": 1,
					"status": 0,
					"numbers": []
		  	},
		  	{
		  		"drawid": 1014560,
					"draw_time": "2020/10/25 10:45",
					"gametypeid": 10001,
					"current_draw": 0,
					"status": 200,
					"numbers": ["7","6","8","1","10"]
		  	},
		  	{
		  		"drawid": 1014559,
					"draw_time": "2020/10/25 10:40",
					"gametypeid": 10001,
					"current_draw": 0,
					"status": 400,
					"numbers": ["2","4","6","9","7"]
		  	},
		  	{
		  		"drawid": 1014558,
					"draw_time": "2020/10/25 10:35",
					"gametypeid": 10001,
					"current_draw": 0,
					"status": 400,
					"numbers": ["2","11","4","7","3"]
		  	},
		  	{
		  		"drawid": 1014557,
		  		"draw_time": "2020/10/25 10:30",
					"gametypeid": 10001,
					"current_draw": 0,
					"status": 400,
					"numbers": ["7","8","9","2","3"]
		  	},
		  	{
		  		"drawid": 1014556,
					"draw_time": "2020/10/25 10:25",
					"gametypeid": 10001,
					"current_draw": 0,
					"status": 400,
					"numbers": ["4","9","6","1","2"]
		  	}
	  	]
	  }
	})
}

export const getOrderList = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
    "msg":"Please log in to your account before playing",
	  "data": {
  		"casinobalance": 358,
			"lottobalance": 158,
			"orderid": 1000343,
			"realmoney": 158
	  }
	})
}

export const getPayoutInfo = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
	  "data": {
	  	"drawid": 124545,
	    "draw_time": '2020/10/20 13:15',
	    "numbers": [1,2,3,5,9]
	  }
	})
}

export const getPayoutList = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
	  "data": [
		  {
		  	"divisions": 1, 
		  	"match": '5', 
		  	"odds": '1:462', 
		  	"multiplier": '290'
		  },
	    {
	    	"divisions": 2, 
	    	"match": '4', 
	    	"odds": '1:66', 
	    	"multiplier": '40'
	    },
	    {
	    	"divisions": 3, 
	    	"match": '3', 
	    	"odds": '1:17', 
	    	"multiplier": '10'
	    }
	  ]
	})
}

export const getPayoutNumber = (option) => {
	let { params } = JSON.parse(option.body);
	console.log(params);
	if (params == '2020/10/20 00:00') {
		return {
			"status": 0,
	    "code": 0,
		  "data": {
		  	"numbers": [10,6,4,8,17]
		  }
		}
	} else if (params == '2020/10/20 00:05') {
		return {
			"status": 0,
	    "code": 0,
		  "data": {
		  	"numbers": [20,3,8,1,82]
		  }
		}
	} else if (params == '2020/10/21 00:00') {
		return {
			"status": 0,
	    "code": 0,
		  "data": {
		  	"numbers": [8,1,4,7,27]
		  }
		}
	} else if (params == '2020/10/21 00:05') {
		return {
			"status": 0,
	    "code": 0,
		  "data": {
		  	"numbers": []
		  }
		}
	} else {
		return {
			"status": 0,
	    "code": 0,
		  "data": {
		  	"numbers": []
		  }
		}
	}
}

export const getWeekList = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
	  "data": {
  	  "select": {
        "week1": "week44",
        "week2": "week43",
        "week3": "week42"
      },
      "week1": [
        {
          "no": 1,
          "name": "G*** V**",
          "winning": "€145.00"
        },
        {
          "no": 2,
          "name": "A*** G**",
          "winning": "€20.00"
        },
        {
          "no": 3,
          "name": "A*** G**",
          "winning": "€20.00"
        },
        {
          "no": 4,
          "name": "A*** G**",
          "winning": "€15.00"
        },
        {
          "no": 5,
          "name": "A*** G**",
          "winning": "€10.00"
        },
        {
          "no": 6,
          "name": "A*** G**",
          "winning": "€10.00"
        },
        {
          "no": 7,
          "name": "A*** G**",
          "winning": "€10.00"
        },
        {
          "no": 8,
          "name": "k*** p**",
          "winning": "€10.00"
        },
        {
          "no": 9,
          "name": "k*** p**",
          "winning": "€10.00"
        },
        {
          "no": 10,
          "name": "A*** G**",
          "winning": "€10.00"
        }
      ],
      "week2": [
        {
          "no": 1,
          "name": "G*** V**",
          "winning": "€145.00"
        },
        {
          "no": 2,
          "name": "S*** G**",
          "winning": "€29.00"
        },
        {
          "no": 3,
          "name": "S*** G**",
          "winning": "€29.00"
        },
        {
          "no": 4,
          "name": "S*** G**",
          "winning": "€29.00"
        },
        {
          "no": 5,
          "name": "M*** J**",
          "winning": "€20.00"
        },
        {
          "no": 6,
          "name": "h*** k**",
          "winning": "€10.00"
        },
        {
          "no": 7,
          "name": "k*** p**",
          "winning": "€10.00"
        },
        {
          "no": 8,
          "name": "k*** p**",
          "winning": "€10.00"
        },
        {
          "no": 9,
          "name": "k*** p**",
          "winning": "€10.00"
        },
        {
          "no": 10,
          "name": "k*** p**",
          "winning": "€8.00"
        }
      ],
      "week3": [
        {
          "no": 1,
          "name": "G*** K**",
          "winning": "€145.00"
        },
        {
          "no": 2,
          "name": "S*** G**",
          "winning": "€29.00"
        },
        {
          "no": 3,
          "name": "S*** G**",
          "winning": "€29.00"
        },
        {
          "no": 4,
          "name": "W*** T**",
          "winning": "€20.00"
        },
        {
          "no": 5,
          "name": "D*** P**",
          "winning": "€20.00"
        },
        {
          "no": 6,
          "name": "B*** A**",
          "winning": "€20.00"
        },
        {
          "no": 7,
          "name": "D*** P**",
          "winning": "€12.00"
        },
        {
          "no": 8,
          "name": "D*** P**",
          "winning": "€11.00"
        },
        {
          "no": 9,
          "name": "D*** P**",
          "winning": "€10.00"
        },
        {
          "no": 10,
          "name": "W*** T**",
          "winning": "€10.00"
        }
      ]
	  }
	})
}

export const getBetsList = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
    "data": [
      {
        "draw_date": "2020/10/27",
        "list": [
          {
            "draw_time": "10/27 09:30",
            "orderid": 1000343,
            "orders_items_draws_id": 1003614,
            "numbers": [
              {
                "number": "6",
                "hit": 1
              },
              {
                "number": "7",
                "hit": 1
              },
              {
                "number": "8",
                "hit": 0
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1020982,
            "userid": 617387,
            "betting_time": "2020/10/27 09:28:21",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          }
        ]
      },
      {
        "draw_date": "2020/10/26",
        "list": [
          {
            "draw_time": "10/26 12:35",
            "orderid": 1000342,
            "orders_items_draws_id": 1003613,
            "numbers": [
              {
                "number": "4",
                "hit": 1
              },
              {
                "number": "6",
                "hit": 1
              },
              {
                "number": "10",
                "hit": 0
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1020731,
            "userid": 617387,
            "betting_time": "2020/10/26 12:01:38",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          }
        ]
      },
      {
        "draw_date": "2020/08/28",
        "list": [
          {
            "draw_time": "08/28 16:40",
            "orderid": 1000279,
            "orders_items_draws_id": 1002369,
            "numbers": [
              {
                "number": "5",
                "hit": 1
              },
              {
                "number": "7",
                "hit": 1
              },
              {
                "number": "8",
                "hit": 1
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1004496,
            "userid": 617387,
            "betting_time": "2020/08/28 16:35:01",
            "winning": "€5.00",
            "status": 200,
            "statusText": "€5.00",
            "is_win": 1
          },
          {
            "draw_time": "08/28 16:40",
            "orderid": 1000279,
            "orders_items_draws_id": 1002368,
            "numbers": [
              {
                "number": "3",
                "hit": 0
              },
              {
                "number": "4",
                "hit": 0
              },
              {
                "number": "8",
                "hit": 1
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1004496,
            "userid": 617387,
            "betting_time": "2020/08/28 16:35:01",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          },
          {
            "draw_time": "08/28 16:40",
            "orderid": 1000279,
            "orders_items_draws_id": 1002367,
            "numbers": [
              {
                "number": "1",
                "hit": 1
              },
              {
                "number": "7",
                "hit": 1
              },
              {
                "number": "9",
                "hit": 0
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1004496,
            "userid": 617387,
            "betting_time": "2020/08/28 16:35:01",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          },
          {
            "draw_time": "08/28 16:35",
            "orderid": 1000279,
            "orders_items_draws_id": 1002366,
            "numbers": [
              {
                "number": "5",
                "hit": 0
              },
              {
                "number": "7",
                "hit": 1
              },
              {
                "number": "8",
                "hit": 1
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1004495,
            "userid": 617387,
            "betting_time": "2020/08/28 16:30:02",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          },
          {
            "draw_time": "08/28 16:35",
            "orderid": 1000279,
            "orders_items_draws_id": 1002365,
            "numbers": [
              {
                "number": "3",
                "hit": 0
              },
              {
                "number": "4",
                "hit": 0
              },
              {
                "number": "8",
                "hit": 1
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1004495,
            "userid": 617387,
            "betting_time": "2020/08/28 16:30:02",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          },
          {
            "draw_time": "08/28 16:35",
            "orderid": 1000279,
            "orders_items_draws_id": 1002364,
            "numbers": [
              {
                "number": "1",
                "hit": 1
              },
              {
                "number": "7",
                "hit": 1
              },
              {
                "number": "9",
                "hit": 0
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1004495,
            "userid": 617387,
            "betting_time": "2020/08/28 16:30:01",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          },
          {
            "draw_time": "08/28 16:30",
            "orderid": 1000279,
            "orders_items_draws_id": 1002363,
            "numbers": [
              {
                "number": "5",
                "hit": 1
              },
              {
                "number": "7",
                "hit": 0
              },
              {
                "number": "8",
                "hit": 0
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1004494,
            "userid": 617387,
            "betting_time": "2020/08/28 16:25:01",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          },
          {
            "draw_time": "08/28 16:30",
            "orderid": 1000279,
            "orders_items_draws_id": 1002362,
            "numbers": [
              {
                "number": "3",
                "hit": 0
              },
              {
                "number": "4",
                "hit": 1
              },
              {
                "number": "8",
                "hit": 0
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1004494,
            "userid": 617387,
            "betting_time": "2020/08/28 16:25:01",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          }
        ]
      }
    ]
	})
}

export const addBetsList = () => {
	return Mock.mock({
		"status": 0,
    "code": 0,
    "data": [
      {
        "draw_date": "2020/10/27",
        "list": [
          {
            "draw_time": "11/2 09:30",
            "orderid": 1000116,
            "orders_items_draws_id": 1003678,
            "numbers": [
              {
                "number": "6",
                "hit": 1
              },
              {
                "number": "7",
                "hit": 1
              },
              {
                "number": "8",
                "hit": 0
              }
            ],
            "amount": "€0.50",
            "currency": "EUR",
            "drawid": 1020982,
            "userid": 617387,
            "betting_time": "2020/11/2 09:28:21",
            "winning": "€0.00",
            "status": 200,
            "statusText": "€0.00",
            "is_win": 0
          }
        ]
      }
    ]
	})
}

export const getCasinoInfo = () => {
	return Mock.mock({
		"status":0,
    "code":0,
    "data": {
    	"gategory":[
	    	{
	    		"name":"All",
	    		"type":"all"
	    	},
	    	{
	    		"name":"New",
	    		"type":"new"
	    	},
	    	{
	    		"name":"Slots",
	    		"type":"slots"
	    	},
	    	{	
	    		"name":"Jackpot",
	    		"type":"jackpot"
	    	},
	    	{
	    		"name":"Table games",
	    		"type":"games"
	    	}
    	],
	    "provider": [
	      {
		    	"type":"all",
		    	"list":[
			    	{
			    		'name':'All'
			    	}
		    	]
		    },
		    {
		    	"type":"new",
		    	"list":[
			    	{
			    		'name':'All',
			    		'num':10
			    	},
			    	{
			    		'name':'Relax Gaming',
			    		'num':7
			    	},
			    	{
			    		'name':'Playson',
			    		'num':3
			    	}
		    	]
		    },
		    {
		    	"type":"slots",
		    	"list":[
		    	  {
			    		'name':'All',
			    		'num':70
			    	},
			    	{
			    		'name':'Microgaming',
			    		'num':18
			    	},
			    	{
			    		'name':'Playson',
			    		'num':32
			    	},
			    	{
			    		'name':'GameHub',
			    		'num':7
			    	},
			    	{
			    		'name':'Hacksaw',
			    		'num':7
			    	},
			    	{
			    		'name':'Endorphina',
			    		'num':6
			    	}
		    	]
		    },
	      {
		    	"type":"jackpot",
		    	"list":[
		    	  {
			    		'name':'All',
			    		'num':4
			    	},
			    	{
			    		'name':'Microgaming',
			    		'num':4
			    	}
		    	]
		    },
		    {
		    	"type":"games",
		    	"list":[
		    	  {
			    		'name':'All',
			    		'num':2
			    	},
			    	{
			    		'name':'Relax Gaming',
			    		'num':1
			    	},
			    	{
			    		'name':'Microgaming',
			    		'num':1
			    	}
		    	]
		    }
	    ],
	    "game": [
		    {
		    	"type":"all",
		    	"list": [
    		    {
							"type":"new",
							"list": [
								{
									'name':'Sakura Fortune',
									'provider':'Relax Gaming',
									'play_url':'sakurafortune',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/sakurafortune.png',
								},
								{
									'name':'Sevens high',
									'provider':'Relax Gaming',
									'play_url':'sevenshigh',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/sevenshigh.png',
								},
								{
									'name':'Sugar Trail',
									'provider':'Relax Gaming',
									'play_url':'sugartrail',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/sugartrail.png',
								},
								{
									'name':'King Colossus',
									'provider':'Relax Gaming',
									'play_url':'kingcolossus',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/kingcolossus.png',
								},
								{
									'name':'Second Strike',
									'provider':'Relax Gaming',
									'play_url':'secondstrike',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/secondstrike.png',
								},
								{
									'name':'Crystal Queen',
									'provider':'Relax Gaming',
									'play_url':'crystalqueen',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/crystalqueen.png',
								},
								{
									'name':'Spinions',
									'provider':'Relax Gaming',
									'play_url':'spinions',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/spinions.png',
								},
				    		{
									'name':'Bumper Crop',
									'provider':'Playson',
									'play_url':'bumpercrop',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/bumpercrop.png',
								},
								{
									'name':'Juice\'n\'Fruits',
									'provider':'Playson',
									'play_url':'juiceandfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/juicenfruits.png',
								},
								{
									'name':'Alice in Wonderslots',
									'provider':'Playson',
									'play_url':'aliceinwonderslots',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5bab484e60360.png',
								}
							]
						},
						{
							"type":"slots",
							"list":[
							  {
									'name':'Thunderstruck',
									'provider':'Microgaming',
									'play_url':'thunderstruck',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/thunderstruck.png',
								},
								{
									'name':'Lost Vegas',
									'provider':'Microgaming',
									'play_url':'lostvegas',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/lostvegas.png',
								},
								{
									'name':'RugbyStar',
									'provider':'Microgaming',
									'play_url':'rugbystar',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/rugbystar.png',
								},
								{
									'name':'PokerPursuit',
									'provider':'Microgaming',
									'play_url':'pokerpursuit',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playngo/thumbnails/matsuri.png',
								},
								{
									'name':'Emperor of the Sea',
									'provider':'Microgaming',
									'play_url':'emperorofthesea',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/emperorofthesea.png',
								},
								{
									'name':'Huangdi - Yellow Emperor',
									'provider':'Microgaming',
									'play_url':'huangdiyellowemperor',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/huangdi.png',
								},
								{
									'name':'Dragonz',
									'provider':'Microgaming',
									'play_url':'dragonz',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/dragonz.png',
								},
								{
									'name':'Hot As Hades',
									'provider':'Microgaming',
									'play_url':'hotashades',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/hotashades.png',
								},
								{
									'name':'Hellboy',
									'provider':'Microgaming',
									'play_url':'hellboy',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/hellboy.png',
								},
								{
									'name':'Immortal Romance',
									'provider':'Microgaming',
									'play_url':'immortalromance',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/immortalromance.png',
								},
								{
									'name':'Thunderstruck 2',
									'provider':'Microgaming',
									'play_url':'thunderstruck2',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/59fc41aa82617.png',
								},
								{
									'name':'Cash Pig',
									'provider':'Microgaming',
									'play_url':'cashpig',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60425df39183f.png',
								},
								{
									'name':'Halloween ®',
									'provider':'Microgaming',
									'play_url':'halloween',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f4379ec97159.png',
								},
								{
									'name':'Anderthals ™',
									'provider':'Microgaming',
									'play_url':'anderthals',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5e94227f82b4f.png',
								},
								{
									'name':'Diamond Force',
									'provider':'Microgaming',
									'play_url':'diamondforce',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5e9420af0041b.png',
								},
								{
									'name':'Mining Fever',
									'provider':'Microgaming',
									'play_url':'miningfever',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5e7b3959448b0.png',
								},
								{
									'name':'The Smashing Biscuit',
									'provider':'Microgaming',
									'play_url':'thesmashingbiscuit',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5d024df97f366.jpg',
								},
								{
									'name':'Reel Splitter',
									'provider':'Microgaming',
									'play_url':'reelsplitter',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5cdd6b06b1b97.jpg',
								},
								{
									'name':'Alice in Wonderslots',
									'provider':'Playson',
									'play_url':'aliceinwonderslots',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/aliceinwonderslots.png',
								},
								{
									'name':'Aquatica',
									'provider':'Playson',
									'play_url':'aquatica',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/aquatica.png',
								},
								{
									'name':'Burlesque Queen',
									'provider':'Playson',
									'play_url':'burlesquequeen',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/burlesquequeen.png',
								},
								{
									'name':'Bumper Crop',
									'provider':'Playson',
									'play_url':'bumpercrop',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/bumpercrop.png',
								},
								{
									'name':'Circus Deluxe',
									'provider':'Playson',
									'play_url':'circusdeluxe',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/circusdeluxe.png',
								},
								{
									'name':'Dracula\'s Family',
									'provider':'Playson',
									'play_url':'draculasfamily',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/draculasfamily.png',
								},
								{
									'name':'Fruits of the Nile',
									'provider':'Playson',
									'play_url':'fruitsofthenile',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/fruitsofthenile.png',
								},
								{
									'name':'Fruits\'n\'Stars',
									'provider':'Playson',
									'play_url':'fruitsnstars',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/fruitsnstars.png',
								},
								{
									'name':'Gold Rush',
									'provider':'Playson',
									'play_url':'goldrush',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/goldrush.png',
								},
								{
									'name':'Happy Jungle Deluxe',
									'provider':'Playson',
									'play_url':'happyjungledeluxe',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/happyjungle.png',
								},
								{
									'name':'Juice\'n\'Fruits',
									'provider':'Playson',
									'play_url':'juicenfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/juicenfruits.png',
								},
								{
									'name':'Lucky Birds',
									'provider':'Playson',
									'play_url':'luckybirds',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/luckybirds.png',
								},
								{
									'name':'Lucky Pirates',
									'provider':'Playson',
									'play_url':'luckypirates',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/luckypirates.png',
								},
								{
									'name':'Lucky Reels',
									'provider':'Playson',
									'play_url':'luckyreels',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/luckyreels.png',
								},
								{
									'name':'Magic Forest',
									'provider':'Playson',
									'play_url':'magicforest',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/magicforest.png',
								},
								{
									'name':'Mysteries of Notre Dames',
									'provider':'Playson',
									'play_url':'mysteriesofnotredames',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/mysteries_of_notre_dame.png',
								},
								{
									'name':'Odysseus',
									'provider':'Playson',
									'play_url':'odysseus',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/odysseus.png',
								},
								{
									'name':'Pirate\'s Treasures Deluxe',
									'provider':'Playson',
									'play_url':'piratestreasuresdeluxe',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/piratetreasuredeluxe.png',
								},
								{
									'name':'Riches of Cleopatra',
									'provider':'Playson',
									'play_url':'richesofcleopatra',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/richesofcleopatra.png',
								},
								{
									'name':'Sky Way',
									'provider':'Playson',
									'play_url':'skyway',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/skyway.png',
								},
								{
									'name':'Spirits of Aztec',
									'provider':'Playson',
									'play_url':'spiritsofaztec',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/spirits_of_aztec.png',
								},
								{
									'name':'Thunder Reels',
									'provider':'Playson',
									'play_url':'thunderreels',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/thunderreels.png',
								},
								{
									'name':'Treasures of Tombs',
									'provider':'Playson',
									'play_url':'treasuresoftombs',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/treasuresoftombs.png',
								},
								{
									'name':'Wild Hunter',
									'provider':'Playson',
									'play_url':'wildhunter',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/wildhunter.png',
								},
								{
									'name':'Zombirthday',
									'provider':'Playson',
									'play_url':'zombirthday',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/zombirthday.png',
								},
								{
									'name':'Space Corsairs',
									'provider':'Playson',
									'play_url':'spacecorsairs',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/space_corsairs.png',
								},
								{
									'name':'Eastern Delights',
									'provider':'Playson',
									'play_url':'easterndelights',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/eastern_delights.png',
								},
								{
									'name':'Taiga',
									'provider':'Playson',
									'play_url':'taiga',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/taiga.png',
								},
								{
									'name':'Aztec Empire',
									'provider':'Playson',
									'play_url':'aztecempire',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/aztec_empire.png',
								},
								{
									'name':'Art of the Heist',
									'provider':'Playson',
									'play_url':'artoftheheist',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/artoftheheist.png',
								},
								{
									'name':'Down the Pub',
									'provider':'Playson',
									'play_url':'downthepub',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/downthepub.png',
								},
								{
									'name':'SpellCraft',
									'provider':'Playson',
									'play_url':'spellcraft',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/spellcraft.png',
								},
								{
									'name':'HOT Fruits',
									'provider':'GameHub',
									'play_url':'hotfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602b93e660739.png',
								},
								{
									'name':'FruitCocktail7',
									'provider':'GameHub',
									'play_url':'fruitcocktail7',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602a98a907930.png',
								},
								{
									'name':'Meme Faces',
									'provider':'GameHub',
									'play_url':'memefaces',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602a98499d519.png',
								},
								{
									'name':'Super Dragons Fire',
									'provider':'GameHub',
									'play_url':'superdragonsfire',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602a97527ecc2.png',
								},
								{
									'name':'Wild7Fruits',
									'provider':'GameHub',
									'play_url':'wild7fruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60143b1d1499d.png',
								},
								{
									'name':'MarsDinner',
									'provider':'GameHub',
									'play_url':'marsdinner',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60143a63c9e79.png',
								},
								{
									'name':'She/He_beach',
									'provider':'GameHub',
									'play_url':'shemebeach',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60059e69176bd.png',
								},
								{
									'name':'Chaos Crew',
									'provider':'Hacksaw',
									'play_url':'chaoscrew',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dc9705f690.png',
								},
								{
									'name':'The Respinners',
									'provider':'Hacksaw',
									'play_url':'therespinners',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcc07a2a00.png',
								},
								{
									'name':'Miami Multiplier',
									'provider':'Hacksaw',
									'play_url':'miamimultiplier',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcc704a1ab.png',
								},
								{
									'name':'Cash Compass',
									'provider':'Hacksaw',
									'play_url':'cashcompass',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcd6876c61.png',
								},
								{
									'name':'Cubes',
									'provider':'Hacksaw',
									'play_url':'cubes',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcd04f0e4d.png',
								},
								{
									'name':'OmNom',
									'provider':'Hacksaw',
									'play_url':'omNom',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcacdd07e9.png',
								},
								{
									'name':'Stick\'em',
									'provider':'Hacksaw',
									'play_url':'etickem',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcb6c7a160.jpg',
								},
								{
									'name':'Geisha',
									'provider':'endorphina',
									'play_url':'geisha',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Fresh Fruits',
									'provider':'endorphina',
									'play_url':'freshfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Football',
									'provider':'endorphina',
									'play_url':'football',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Chimney Sweep',
									'provider':'endorphina',
									'play_url':'chimneysweep',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Blast Boom Bang',
									'provider':'endorphina',
									'play_url':'blastboombang',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'4 of a King',
									'provider':'endorphina',
									'play_url':'4ofaking',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								}
							]
						},
						{
							"type":"jackpot",
							"list":[
							  {
									'name':'Mega Moolah',
									'provider':'Microgaming',
									'play_url':'megamoolah',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/59ef2c05d767d.png',
								},
								{
									'name':'Major Millions™',
									'provider':'Microgaming',
									'play_url':'majormillions',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/majormillions.png',
								},
								{
									'name':'Tunzamunni',
									'provider':'Microgaming',
									'play_url':'tunzamunni',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/redrake/thumbnails/myrtlethewitch.png',
								},
								{
									'name':'KingCashalot',
									'provider':'Microgaming',
									'play_url':'kingCashalot',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/redrake/thumbnails/myrtlethewitch.png',
								}
							]
						},
						{
							"type":"games",
							"list":[
							  {
									'name':'Blackjack+',
									'provider':'Relax Gaming',
									'play_url':'blackjackplus',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5ed7013e6ede4.png',
								},
			    			{
									'name':'Baccarat',
									'provider':'Microgaming',
									'play_url':'baccarat',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/redrake/thumbnails/myrtlethewitch.png',
								}
							]
						}
		    	]
		    },
	      {
		    	"type":"new",
		    	"list": [
			    	{
			    		'name':'All',
			    		"list": [
			    		  {
									'name':'Sakura Fortune',
									'provider':'Relax Gaming',
									'play_url':'sakurafortune',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/sakurafortune.png',
								},
								{
									'name':'Sevens high',
									'provider':'Relax Gaming',
									'play_url':'sevenshigh',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/sevenshigh.png',
								},
								{
									'name':'Sugar Trail',
									'provider':'Relax Gaming',
									'play_url':'sugartrail',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/sugartrail.png',
								},
								{
									'name':'King Colossus',
									'provider':'Relax Gaming',
									'play_url':'kingcolossus',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/kingcolossus.png',
								},
								{
									'name':'Second Strike',
									'provider':'Relax Gaming',
									'play_url':'secondstrike',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/secondstrike.png',
								},
								{
									'name':'Crystal Queen',
									'provider':'Relax Gaming',
									'play_url':'crystalqueen',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/crystalqueen.png',
								},
								{
									'name':'Spinions',
									'provider':'Relax Gaming',
									'play_url':'spinions',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/spinions.png',
								},
				    		{
									'name':'Bumper Crop',
									'provider':'Playson',
									'play_url':'bumpercrop',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/bumpercrop.png',
								},
								{
									'name':'Juice\'n\'Fruits',
									'provider':'Playson',
									'play_url':'juiceandfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/juicenfruits.png',
								},
								{
									'name':'Alice in Wonderslots',
									'provider':'Playson',
									'play_url':'aliceinwonderslots',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5bab484e60360.png',
								}
			    		]
			    	},
			    	{
			    		'name':'Relax Gaming',
			    		"list": [
				    		{
									'name':'Sakura Fortune',
									'provider':'Relax Gaming',
									'play_url':'sakurafortune',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/sakurafortune.png',
								},
								{
									'name':'Sevens high',
									'provider':'Relax Gaming',
									'play_url':'sevenshigh',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/sevenshigh.png',
								},
								{
									'name':'Sugar Trail',
									'provider':'Relax Gaming',
									'play_url':'sugartrail',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/sugartrail.png',
								},
								{
									'name':'King Colossus',
									'provider':'Relax Gaming',
									'play_url':'kingcolossus',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/kingcolossus.png',
								},
								{
									'name':'Second Strike',
									'provider':'Relax Gaming',
									'play_url':'secondstrike',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/secondstrike.png',
								},
								{
									'name':'Crystal Queen',
									'provider':'Relax Gaming',
									'play_url':'crystalqueen',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/crystalqueen.png',
								},
								{
									'name':'Spinions',
									'provider':'Relax Gaming',
									'play_url':'spinions',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/spinions.png',
								}
			    		]
			    	},
			    	{
			    		'name':'Playson',
			    		"list": [
				    		{
									'name':'Bumper Crop',
									'provider':'Playson',
									'play_url':'bumpercrop',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/bumpercrop.png',
								},
								{
									'name':'Juice\'n\'Fruits',
									'provider':'Playson',
									'play_url':'juiceandfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/juicenfruits.png',
								},
								{
									'name':'Alice in Wonderslots',
									'provider':'Playson',
									'play_url':'aliceinwonderslots',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5bab484e60360.png',
								}
			    		]
			    	}
		    	]
		    },
	      {
		    	"type":"slots",
		    	"list": [
			    	{
						'name':'All',
						'list': [
								{
									'name':'Thunderstruck',
									'provider':'Microgaming',
									'play_url':'thunderstruck',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/thunderstruck.png',
								},
								{
									'name':'Lost Vegas',
									'provider':'Microgaming',
									'play_url':'lostvegas',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/lostvegas.png',
								},
								{
									'name':'RugbyStar',
									'provider':'Microgaming',
									'play_url':'rugbystar',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/rugbystar.png',
								},
								{
									'name':'PokerPursuit',
									'provider':'Microgaming',
									'play_url':'pokerpursuit',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playngo/thumbnails/matsuri.png',
								},
								{
									'name':'Emperor of the Sea',
									'provider':'Microgaming',
									'play_url':'emperorofthesea',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/emperorofthesea.png',
								},
								{
									'name':'Huangdi - Yellow Emperor',
									'provider':'Microgaming',
									'play_url':'huangdiyellowemperor',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/huangdi.png',
								},
								{
									'name':'Dragonz',
									'provider':'Microgaming',
									'play_url':'dragonz',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/dragonz.png',
								},
								{
									'name':'Hot As Hades',
									'provider':'Microgaming',
									'play_url':'hotashades',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/hotashades.png',
								},
								{
									'name':'Hellboy',
									'provider':'Microgaming',
									'play_url':'hellboy',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/hellboy.png',
								},
								{
									'name':'Immortal Romance',
									'provider':'Microgaming',
									'play_url':'immortalromance',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/immortalromance.png',
								},
								{
									'name':'Thunderstruck 2',
									'provider':'Microgaming',
									'play_url':'thunderstruck2',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/59fc41aa82617.png',
								},
								{
									'name':'Cash Pig',
									'provider':'Microgaming',
									'play_url':'cashpig',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60425df39183f.png',
								},
								{
									'name':'Halloween ®',
									'provider':'Microgaming',
									'play_url':'halloween',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f4379ec97159.png',
								},
								{
									'name':'Anderthals ™',
									'provider':'Microgaming',
									'play_url':'anderthals',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5e94227f82b4f.png',
								},
								{
									'name':'Diamond Force',
									'provider':'Microgaming',
									'play_url':'diamondforce',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5e9420af0041b.png',
								},
								{
									'name':'Mining Fever',
									'provider':'Microgaming',
									'play_url':'miningfever',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5e7b3959448b0.png',
								},
								{
									'name':'The Smashing Biscuit',
									'provider':'Microgaming',
									'play_url':'thesmashingbiscuit',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5d024df97f366.jpg',
								},
								{
									'name':'Reel Splitter',
									'provider':'Microgaming',
									'play_url':'reelsplitter',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5cdd6b06b1b97.jpg',
								},
								{
									'name':'Alice in Wonderslots',
									'provider':'Playson',
									'play_url':'aliceinwonderslots',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/aliceinwonderslots.png',
								},
								{
									'name':'Aquatica',
									'provider':'Playson',
									'play_url':'aquatica',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/aquatica.png',
								},
								{
									'name':'Burlesque Queen',
									'provider':'Playson',
									'play_url':'burlesquequeen',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/burlesquequeen.png',
								},
								{
									'name':'Bumper Crop',
									'provider':'Playson',
									'play_url':'bumpercrop',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/bumpercrop.png',
								},
								{
									'name':'Circus Deluxe',
									'provider':'Playson',
									'play_url':'circusdeluxe',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/circusdeluxe.png',
								},
								{
									'name':'Dracula\'s Family',
									'provider':'Playson',
									'play_url':'draculasfamily',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/draculasfamily.png',
								},
								{
									'name':'Fruits of the Nile',
									'provider':'Playson',
									'play_url':'fruitsofthenile',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/fruitsofthenile.png',
								},
								{
									'name':'Fruits\'n\'Stars',
									'provider':'Playson',
									'play_url':'fruitsnstars',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/fruitsnstars.png',
								},
								{
									'name':'Gold Rush',
									'provider':'Playson',
									'play_url':'goldrush',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/goldrush.png',
								},
								{
									'name':'Happy Jungle Deluxe',
									'provider':'Playson',
									'play_url':'happyjungledeluxe',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/happyjungle.png',
								},
								{
									'name':'Juice\'n\'Fruits',
									'provider':'Playson',
									'play_url':'juicenfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/juicenfruits.png',
								},
								{
									'name':'Lucky Birds',
									'provider':'Playson',
									'play_url':'luckybirds',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/luckybirds.png',
								},
								{
									'name':'Lucky Pirates',
									'provider':'Playson',
									'play_url':'luckypirates',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/luckypirates.png',
								},
								{
									'name':'Lucky Reels',
									'provider':'Playson',
									'play_url':'luckyreels',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/luckyreels.png',
								},
								{
									'name':'Magic Forest',
									'provider':'Playson',
									'play_url':'magicforest',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/magicforest.png',
								},
								{
									'name':'Mysteries of Notre Dames',
									'provider':'Playson',
									'play_url':'mysteriesofnotredames',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/mysteries_of_notre_dame.png',
								},
								{
									'name':'Odysseus',
									'provider':'Playson',
									'play_url':'odysseus',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/odysseus.png',
								},
								{
									'name':'Pirate\'s Treasures Deluxe',
									'provider':'Playson',
									'play_url':'piratestreasuresdeluxe',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/piratetreasuredeluxe.png',
								},
								{
									'name':'Riches of Cleopatra',
									'provider':'Playson',
									'play_url':'richesofcleopatra',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/richesofcleopatra.png',
								},
								{
									'name':'Sky Way',
									'provider':'Playson',
									'play_url':'skyway',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/skyway.png',
								},
								{
									'name':'Spirits of Aztec',
									'provider':'Playson',
									'play_url':'spiritsofaztec',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/spirits_of_aztec.png',
								},
								{
									'name':'Thunder Reels',
									'provider':'Playson',
									'play_url':'thunderreels',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/thunderreels.png',
								},
								{
									'name':'Treasures of Tombs',
									'provider':'Playson',
									'play_url':'treasuresoftombs',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/treasuresoftombs.png',
								},
								{
									'name':'Wild Hunter',
									'provider':'Playson',
									'play_url':'wildhunter',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/wildhunter.png',
								},
								{
									'name':'Zombirthday',
									'provider':'Playson',
									'play_url':'zombirthday',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/zombirthday.png',
								},
								{
									'name':'Space Corsairs',
									'provider':'Playson',
									'play_url':'spacecorsairs',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/space_corsairs.png',
								},
								{
									'name':'Eastern Delights',
									'provider':'Playson',
									'play_url':'easterndelights',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/eastern_delights.png',
								},
								{
									'name':'Taiga',
									'provider':'Playson',
									'play_url':'taiga',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/taiga.png',
								},
								{
									'name':'Aztec Empire',
									'provider':'Playson',
									'play_url':'aztecempire',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/aztec_empire.png',
								},
								{
									'name':'Art of the Heist',
									'provider':'Playson',
									'play_url':'artoftheheist',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/artoftheheist.png',
								},
								{
									'name':'Down the Pub',
									'provider':'Playson',
									'play_url':'downthepub',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/downthepub.png',
								},
								{
									'name':'SpellCraft',
									'provider':'Playson',
									'play_url':'spellcraft',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/spellcraft.png',
								},
								{
									'name':'HOT Fruits',
									'provider':'GameHub',
									'play_url':'hotfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602b93e660739.png',
								},
								{
									'name':'FruitCocktail7',
									'provider':'GameHub',
									'play_url':'fruitcocktail7',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602a98a907930.png',
								},
								{
									'name':'Meme Faces',
									'provider':'GameHub',
									'play_url':'memefaces',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602a98499d519.png',
								},
								{
									'name':'Super Dragons Fire',
									'provider':'GameHub',
									'play_url':'superdragonsfire',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602a97527ecc2.png',
								},
								{
									'name':'Wild7Fruits',
									'provider':'GameHub',
									'play_url':'wild7fruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60143b1d1499d.png',
								},
								{
									'name':'MarsDinner',
									'provider':'GameHub',
									'play_url':'marsdinner',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60143a63c9e79.png',
								},
								{
									'name':'She/He_beach',
									'provider':'GameHub',
									'play_url':'shemebeach',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60059e69176bd.png',
								},
								{
									'name':'Chaos Crew',
									'provider':'Hacksaw',
									'play_url':'chaoscrew',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dc9705f690.png',
								},
								{
									'name':'The Respinners',
									'provider':'Hacksaw',
									'play_url':'therespinners',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcc07a2a00.png',
								},
								{
									'name':'Miami Multiplier',
									'provider':'Hacksaw',
									'play_url':'miamimultiplier',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcc704a1ab.png',
								},
								{
									'name':'Cash Compass',
									'provider':'Hacksaw',
									'play_url':'cashcompass',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcd6876c61.png',
								},
								{
									'name':'Cubes',
									'provider':'Hacksaw',
									'play_url':'cubes',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcd04f0e4d.png',
								},
								{
									'name':'OmNom',
									'provider':'Hacksaw',
									'play_url':'omNom',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcacdd07e9.png',
								},
								{
									'name':'Stick\'em',
									'provider':'Hacksaw',
									'play_url':'etickem',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcb6c7a160.jpg',
								},
								{
									'name':'Geisha',
									'provider':'endorphina',
									'play_url':'geisha',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Fresh Fruits',
									'provider':'endorphina',
									'play_url':'freshfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Football',
									'provider':'endorphina',
									'play_url':'football',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Chimney Sweep',
									'provider':'endorphina',
									'play_url':'chimneysweep',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Blast Boom Bang',
									'provider':'endorphina',
									'play_url':'blastboombang',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'4 of a King',
									'provider':'endorphina',
									'play_url':'4ofaking',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								}
							]
						},
						{
							'name':'Microgaming',
							'list': [
								{
									'name':'Thunderstruck',
									'provider':'Microgaming',
									'play_url':'thunderstruck',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/thunderstruck.png',
								},
								{
									'name':'Lost Vegas',
									'provider':'Microgaming',
									'play_url':'lostvegas',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/lostvegas.png',
								},
								{
									'name':'RugbyStar',
									'provider':'Microgaming',
									'play_url':'rugbystar',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/rugbystar.png',
								},
								{
									'name':'PokerPursuit',
									'provider':'Microgaming',
									'play_url':'pokerpursuit',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playngo/thumbnails/matsuri.png',
								},
								{
									'name':'Emperor of the Sea',
									'provider':'Microgaming',
									'play_url':'emperorofthesea',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/emperorofthesea.png',
								},
								{
									'name':'Huangdi - Yellow Emperor',
									'provider':'Microgaming',
									'play_url':'huangdiyellowemperor',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/huangdi.png',
								},
								{
									'name':'Dragonz',
									'provider':'Microgaming',
									'play_url':'dragonz',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/dragonz.png',
								},
								{
									'name':'Hot As Hades',
									'provider':'Microgaming',
									'play_url':'hotashades',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/hotashades.png',
								},
								{
									'name':'Hellboy',
									'provider':'Microgaming',
									'play_url':'hellboy',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/hellboy.png',
								},
								{
									'name':'Immortal Romance',
									'provider':'Microgaming',
									'play_url':'immortalromance',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/immortalromance.png',
								},
								{
									'name':'Thunderstruck 2',
									'provider':'Microgaming',
									'play_url':'thunderstruck2',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/59fc41aa82617.png',
								},
								{
									'name':'Cash Pig',
									'provider':'Microgaming',
									'play_url':'cashpig',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60425df39183f.png',
								},
								{
									'name':'Halloween ®',
									'provider':'Microgaming',
									'play_url':'halloween',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f4379ec97159.png',
								},
								{
									'name':'Anderthals ™',
									'provider':'Microgaming',
									'play_url':'anderthals',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5e94227f82b4f.png',
								},
								{
									'name':'Diamond Force',
									'provider':'Microgaming',
									'play_url':'diamondforce',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5e9420af0041b.png',
								},
								{
									'name':'Mining Fever',
									'provider':'Microgaming',
									'play_url':'miningfever',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5e7b3959448b0.png',
								},
								{
									'name':'The Smashing Biscuit',
									'provider':'Microgaming',
									'play_url':'thesmashingbiscuit',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5d024df97f366.jpg',
								},
								{
									'name':'Reel Splitter',
									'provider':'Microgaming',
									'play_url':'reelsplitter',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5cdd6b06b1b97.jpg',
								},
							]
						},
						{
							'name':'Playson',
							'list': [
								{
									'name':'Alice in Wonderslots',
									'provider':'Playson',
									'play_url':'aliceinwonderslots',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/aliceinwonderslots.png',
								},
								{
									'name':'Aquatica',
									'provider':'Playson',
									'play_url':'aquatica',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/aquatica.png',
								},
								{
									'name':'Burlesque Queen',
									'provider':'Playson',
									'play_url':'burlesquequeen',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/burlesquequeen.png',
								},
								{
									'name':'Bumper Crop',
									'provider':'Playson',
									'play_url':'bumpercrop',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/bumpercrop.png',
								},
								{
									'name':'Circus Deluxe',
									'provider':'Playson',
									'play_url':'circusdeluxe',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/circusdeluxe.png',
								},
								{
									'name':'Dracula\'s Family',
									'provider':'Playson',
									'play_url':'draculasfamily',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/draculasfamily.png',
								},
								{
									'name':'Fruits of the Nile',
									'provider':'Playson',
									'play_url':'fruitsofthenile',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/fruitsofthenile.png',
								},
								{
									'name':'Fruits\'n\'Stars',
									'provider':'Playson',
									'play_url':'fruitsnstars',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/fruitsnstars.png',
								},
								{
									'name':'Gold Rush',
									'provider':'Playson',
									'play_url':'goldrush',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/goldrush.png',
								},
								{
									'name':'Happy Jungle Deluxe',
									'provider':'Playson',
									'play_url':'happyjungledeluxe',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/happyjungle.png',
								},
								{
									'name':'Juice\'n\'Fruits',
									'provider':'Playson',
									'play_url':'juicenfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/juicenfruits.png',
								},
								{
									'name':'Lucky Birds',
									'provider':'Playson',
									'play_url':'luckybirds',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/luckybirds.png',
								},
								{
									'name':'Lucky Pirates',
									'provider':'Playson',
									'play_url':'luckypirates',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/luckypirates.png',
								},
								{
									'name':'Lucky Reels',
									'provider':'Playson',
									'play_url':'luckyreels',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/luckyreels.png',
								},
								{
									'name':'Magic Forest',
									'provider':'Playson',
									'play_url':'magicforest',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/magicforest.png',
								},
								{
									'name':'Mysteries of Notre Dames',
									'provider':'Playson',
									'play_url':'mysteriesofnotredames',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/mysteries_of_notre_dame.png',
								},
								{
									'name':'Odysseus',
									'provider':'Playson',
									'play_url':'odysseus',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/odysseus.png',
								},
								{
									'name':'Pirate\'s Treasures Deluxe',
									'provider':'Playson',
									'play_url':'piratestreasuresdeluxe',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/piratetreasuredeluxe.png',
								},
								{
									'name':'Riches of Cleopatra',
									'provider':'Playson',
									'play_url':'richesofcleopatra',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/richesofcleopatra.png',
								},
								{
									'name':'Sky Way',
									'provider':'Playson',
									'play_url':'skyway',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/skyway.png',
								},
								{
									'name':'Spirits of Aztec',
									'provider':'Playson',
									'play_url':'spiritsofaztec',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/spirits_of_aztec.png',
								},
								{
									'name':'Thunder Reels',
									'provider':'Playson',
									'play_url':'thunderreels',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/thunderreels.png',
								},
								{
									'name':'Treasures of Tombs',
									'provider':'Playson',
									'play_url':'treasuresoftombs',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/treasuresoftombs.png',
								},
								{
									'name':'Wild Hunter',
									'provider':'Playson',
									'play_url':'wildhunter',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/wildhunter.png',
								},
								{
									'name':'Zombirthday',
									'provider':'Playson',
									'play_url':'zombirthday',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/zombirthday.png',
								},
								{
									'name':'Space Corsairs',
									'provider':'Playson',
									'play_url':'spacecorsairs',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/space_corsairs.png',
								},
								{
									'name':'Eastern Delights',
									'provider':'Playson',
									'play_url':'easterndelights',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/eastern_delights.png',
								},
								{
									'name':'Taiga',
									'provider':'Playson',
									'play_url':'taiga',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/taiga.png',
								},
								{
									'name':'Aztec Empire',
									'provider':'Playson',
									'play_url':'aztecempire',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/aztec_empire.png',
								},
								{
									'name':'Art of the Heist',
									'provider':'Playson',
									'play_url':'artoftheheist',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/artoftheheist.png',
								},
								{
									'name':'Down the Pub',
									'provider':'Playson',
									'play_url':'downthepub',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/downthepub.png',
								},
								{
									'name':'SpellCraft',
									'provider':'Playson',
									'play_url':'spellcraft',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/playson/thumbnails/spellcraft.png',
								}
							]
						},
						{
							'name':'GameHub',
							'list': [
								{
									'name':'HOT Fruits',
									'provider':'GameHub',
									'play_url':'hotfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602b93e660739.png',
								},
								{
									'name':'FruitCocktail7',
									'provider':'GameHub',
									'play_url':'fruitcocktail7',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602a98a907930.png',
								},
								{
									'name':'Meme Faces',
									'provider':'GameHub',
									'play_url':'memefaces',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602a98499d519.png',
								},
								{
									'name':'Super Dragons Fire',
									'provider':'GameHub',
									'play_url':'superdragonsfire',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/602a97527ecc2.png',
								},
								{
									'name':'Wild7Fruits',
									'provider':'GameHub',
									'play_url':'wild7fruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60143b1d1499d.png',
								},
								{
									'name':'MarsDinner',
									'provider':'GameHub',
									'play_url':'marsdinner',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60143a63c9e79.png',
								},
								{
									'name':'She/He_beach',
									'provider':'GameHub',
									'play_url':'shemebeach',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/60059e69176bd.png',
								}
							]
						},
						{
							'name':'Hacksaw',
							'list': [
								{
									'name':'Chaos Crew',
									'provider':'Hacksaw',
									'play_url':'chaoscrew',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dc9705f690.png',
								},
								{
									'name':'The Respinners',
									'provider':'Hacksaw',
									'play_url':'therespinners',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcc07a2a00.png',
								},
								{
									'name':'Miami Multiplier',
									'provider':'Hacksaw',
									'play_url':'miamimultiplier',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcc704a1ab.png',
								},
								{
									'name':'Cash Compass',
									'provider':'Hacksaw',
									'play_url':'cashcompass',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcd6876c61.png',
								},
								{
									'name':'Cubes',
									'provider':'Hacksaw',
									'play_url':'cubes',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcd04f0e4d.png',
								},
								{
									'name':'OmNom',
									'provider':'Hacksaw',
									'play_url':'omNom',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcacdd07e9.png',
								},
								{
									'name':'Stick\'em',
									'provider':'Hacksaw',
									'play_url':'etickem',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcb6c7a160.jpg',
								}
							]
						},
						{
							'name':'Endorphina',
							'list': [
								{
									'name':'Geisha',
									'provider':'endorphina',
									'play_url':'geisha',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Fresh Fruits',
									'provider':'endorphina',
									'play_url':'freshfruits',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Football',
									'provider':'endorphina',
									'play_url':'football',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Chimney Sweep',
									'provider':'endorphina',
									'play_url':'chimneysweep',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'Blast Boom Bang',
									'provider':'endorphina',
									'play_url':'blastboombang',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								},
								{
									'name':'4 of a King',
									'provider':'endorphina',
									'play_url':'4ofaking',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/',
								}
							]
						}
		    	]
		    },
	      {
		    	"type":"jackpot",
		    	"list": [
		    	  {
			    		"name":"All",
			    		"list": [
			    			{
									'name':'Mega Moolah',
									'provider':'Microgaming',
									'play_url':'megamoolah',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/59ef2c05d767d.png',
								},
								{
									'name':'Major Millions™',
									'provider':'Microgaming',
									'play_url':'majormillions',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/majormillions.png',
								},
								{
									'name':'Tunzamunni',
									'provider':'Microgaming',
									'play_url':'tunzamunni',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/redrake/thumbnails/myrtlethewitch.png',
								},
								{
									'name':'KingCashalot',
									'provider':'Microgaming',
									'play_url':'kingCashalot',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/redrake/thumbnails/myrtlethewitch.png',
								}
			    		]
			    	},
			    	{
			    		"name":"Microgaming",
			    		"list": [
			    			{
									'name':'Mega Moolah',
									'provider':'Microgaming',
									'play_url':'megamoolah',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/59ef2c05d767d.png',
								},
								{
									'name':'Major Millions™',
									'provider':'Microgaming',
									'play_url':'majormillions',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/microgaming/thumbnails/majormillions.png',
								},
								{
									'name':'Tunzamunni',
									'provider':'Microgaming',
									'play_url':'tunzamunni',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/redrake/thumbnails/myrtlethewitch.png',
								},
								{
									'name':'KingCashalot',
									'provider':'Microgaming',
									'play_url':'kingCashalot',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/redrake/thumbnails/myrtlethewitch.png',
								}
			    		]
			    	},
		    	]
		    },
		    {
		    	"type":"games",
		    	"list": [
			    	{
			    		"name":"All",
			    		"list": [
			    		  {
									'name':'Blackjack+',
									'provider':'Relax Gaming',
									'play_url':'blackjackplus',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5ed7013e6ede4.png',
								},
			    			{
									'name':'Baccarat',
									'provider':'Microgaming',
									'play_url':'baccarat',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/redrake/thumbnails/myrtlethewitch.png',
								}
			    		]
			    	},
			    	{
			    		"name":"Relax Gaming",
			    		"list": [
			    			{
									'name':'Blackjack+',
									'provider':'Relax Gaming',
									'play_url':'blackjackplus',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5ed7013e6ede4.png',
								}
			    		]
			    	},
			    	{
			    		"name":"Microgaming",
			    		"list": [
			    			{
									'name':'Baccarat',
									'provider':'Microgaming',
									'play_url':'baccarat',
									'logo':'https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/redrake/thumbnails/myrtlethewitch.png',
								}
			    		]
			    	}
		    	]
		    }
	    ]
    }
	})
}

export const getGameDetailInfo = (option) => {
	let casinoInfo = {};
	let { casino_id } = JSON.parse(option.body);
  switch (casino_id) {
  	case 'tnttumble':
      casinoInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name":"TNT Tumble",
			  	"type":['Relax Gaming','Slots'],
			    "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd48525092.png",
			    "background":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/backgrounds/5f6dd48545dd3.png",
			    "demo_url":"tnttumble",
          "play_url":"tnttumble",
			    "describe":"In this progressive 5 reel video slot you can win up to four different jackpots. These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
			  	"address":"https://d2drhksbtcqozo.cloudfront.net/casino/launcher.html?gameid=tnttumble&lang=sv_SE&ticket=&partnerid=249&partner=multilottonet&moneymode=fun&jurisdiction=MT&channel=web"
			  }
			});
      break;
    case 'goldlab':
      casinoInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name":"GoldLab",
			  	"type":['Relax Gaming','Slots'],
			    "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/goldlab.png",
			    "background":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/backgrounds/goldlab.jpg",
			    "demo_url":"goldlab",
          "play_url":"goldlab",
			    "describe":"In this progressive 5 reel video slot you can win up to four different jackpots. These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
			  	"address":"https://d2drhksbtcqozo.cloudfront.net/casino/launcher.html?gameid=goldlab&lang=sv_SE&ticket=&partnerid=249&partner=multilottonet&moneymode=fun&jurisdiction=MT&channel=web"
			  }
			});
      break;
    case 'lafiesta':
      casinoInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name":"La Fiesta",
			  	"type":['Microgaming'],
			    "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd18c46a20.png",
			    "background":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/backgrounds/5f6dd18c71056.png",
			    "demo_url":"lafiesta",
          "play_url":"lafiesta",
			    "describe":"In this progressive 5 reel video slot you can win up to four different jackpots. These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
			  	"address":"https://d2drhksbtcqozo.cloudfront.net/casino/launcher.html?gameid=lafiesta&lang=sv_SE&ticket=&partnerid=249&partner=multilottonet&moneymode=fun&jurisdiction=MT&channel=web"
			  }
			});
      break;
    case 'spaceforce':
      casinoInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name":"space force",
			  	"type":['Microgaming','Slots'],
			    "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/604ecccee8e0a.png",
			    "background":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/backgrounds/604ecccf2104c.png",
			    "demo_url":"spaceforce",
          "play_url":"spaceforce",
			    "describe":"In this progressive 5 reel video slot you can win up to four different jackpots. These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
			  	"address":"https://bornlucky-test-lon.gamevy.com/launch-game?gameId=SPACE_FORCE&currency=EUR&mode=fun&lang=en&operator=multilotto&clientType=desktop"
			  }
			});
      break;
     case 'blackjackplus':
      casinoInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name":"Blackjack+",
			  	"type":['Slots'],
			    "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5ed7013e6ede4.png",
			    "background":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/backgrounds/5ed7013ea7a96.png",
			    "demo_url":"blackjackplus",
          "play_url":"blackjackplus",
			    "describe":"These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
			  	"address":"https://d2drhksbtcqozo.cloudfront.net/casino/launcher.html?gameid=bjplus&lang=en_GB&ticket=&partnerid=249&partner=multilottonet&moneymode=fun&jurisdiction=MT&channel=web"
			  }
			});
      break;
    case 'frogs_scratch':
      casinoInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
		  	  "name":"Frogs Scratch",
		  	  "type":['hacksawgam'],
          "logo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/casino/thumbnails/5e8ed98b75ebb.png",
          "background":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/casino/backgrounds/5e8ed98ba5883.png",
          "demo_url":"frogs_scratch",
          "play_url":"frogs_scratch",
			    "describe":"In this progressive 5 reel video slot you can win up to four different jackpots. These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
			  	"address":"https://static-live.hacksawgaming.com/1051/1.10.0/index.html?language=en&channel=desktop&gameid=1051&mode=2&token=78429620-a31f-d773-3069-50b7e6ec11c5&lobbyurl=https%3A%2F%2Fwww-dev.multilotto.com%2Fen%2Fscratchcards&currency=EUR&partner=multilotto_stg_mt&env=https://rgs-hacksaw-fun-stg.hacksawgaming.com/api"
			  }
			});
      break;
    case 'cash_vault_II_scratch':
      casinoInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
		  	  "name": "Cash Vault II",
		  	  "type":['Microgaming','Slots'],
          "logo":"https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d4801de67f6e.jpg",
          "background":"https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d4801de8c321.png",
          "demo_url":"cash_vault_II_scratch",
          "play_url":"cash_vault_II_scratch",
			    "describe":"In this progressive 5 reel video slot you can win up to four different jackpots. These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
			  	"address":"https://static-live.hacksawgaming.com/1025/1.12.0/index.html?language=en&channel=desktop&gameid=1025&mode=2&token=b3225352-5d17-773a-7898-d94e54e87419&lobbyurl=https%3A%2F%2Fwww-dev.multilotto.com%2Fen%2Fscratchcards&currency=EUR&partner=multilotto_stg_mt&env=https://rgs-hacksaw-fun-stg.hacksawgaming.com/api"
			  }
			});
      break;
    case 'king_treasure_scratch':
      casinoInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
			  	"name": "King Treasure",
		  	  "type":['Microgaming','Slots'],
          "logo": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d47a7a5c23c5.png",
          "background": "https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d47a7a5e997f.png",
          "demo_url":"king_treasure_scratch",
          "play_url":"king_treasure_scratch",
			    "describe":"In this progressive 5 reel video slot you can win up to four different jackpots. These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
          "address":"https://static-live.hacksawgaming.com/1010/1.12.0/index.html?language=en&channel=desktop&gameid=1010&mode=2&token=f159e9df-6efe-55bb-a7b7-8555a659a703&lobbyurl=https%3A%2F%2Fwww-dev.multilotto.com%2Fen%2Fscratchcards&currency=EUR&partner=multilotto_stg_mt&env=https://rgs-hacksaw-fun-stg.hacksawgaming.com/api"
			  }
			});
      break;
    default:
      casinoInfo = Mock.mock({
				"status":0,
			  "code":0,
			  "data":{
		  	  "name": "Cash Vault II",
		  	  "type":['Microgaming','Slots'],
          "demo_url":"cash_vault_II_scratch",
          "play_url":"cash_vault_II_scratch",
			    "describe":"In this progressive 5 reel video slot you can win up to four different jackpots. These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
			  	"address":"https://static-live.hacksawgaming.com/1025/1.12.0/index.html?language=en&channel=desktop&gameid=1025&mode=2&token=b3225352-5d17-773a-7898-d94e54e87419&lobbyurl=https%3A%2F%2Fwww-dev.multilotto.com%2Fen%2Fscratchcards&currency=EUR&partner=multilotto_stg_mt&env=https://rgs-hacksaw-fun-stg.hacksawgaming.com/api",
			  	"logo":"",
          "background":""
			  }
			});
  }
  return casinoInfo;
}

export const getSlideCasinoList = ()=> {
	return Mock.mock({
		"status":0,
    "code":0,
		"data":[
      {
        "id":"1001",
        "name":"space force",
        "type":['Microgaming','Slots'],
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/604ecccee8e0a.png",
        "background":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/backgrounds/604ecccf2104c.png",
        "demo_url":"spaceforce",
        "play_url":"spaceforce",
        "describe":"In this progressive 5 reel video slot you can win up to four different jackpots. ",
        "address":"https://bornlucky-test-lon.gamevy.com/launch-game?gameId=SPACE_FORCE&currency=EUR&mode=fun&lang=en&operator=multilotto&clientType=desktop"
      },
      {
        "id":"1002",
        "name":"Frogs Scratch",
        "type":['hacksawgam'],
        "logo":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/casino/thumbnails/5e8ed98b75ebb.png",
        "background":"https://d3uwcqgr5gxvbk.cloudfront.net/assets/img/casino/backgrounds/5e8ed98ba5883.png",
        "demo_url":"frogs_scratch",
        "play_url":"frogs_scratch",
        "describe":"These are the Mini, Minor, Major and the Mega. The higher you bet, the greater your chances of hitting the really big jackpots!",
        "address":"https://static-live.hacksawgaming.com/1051/1.10.0/index.html?language=en&channel=desktop&gameid=1051&mode=2&token=78429620-a31f-d773-3069-50b7e6ec11c5&lobbyurl=https%3A%2F%2Fwww-dev.multilotto.com%2Fen%2Fscratchcards&currency=EUR&partner=multilotto_stg_mt&env=https://rgs-hacksaw-fun-stg.hacksawgaming.com/api"
      },
      {
        "id":"1003",
        "name": "Cash Vault II",
        "type":['Microgaming','Slots'],
        "logo":"https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/thumbnails\/5d4801de67f6e.jpg",
        "background":"https:\/\/d3uwcqgr5gxvbk.cloudfront.net\/assets\/img\/casino\/backgrounds\/5d4801de8c321.png",
        "demo_url":"cash_vault_II_scratch",
        "play_url":"cash_vault_II_scratch",
        "describe":"The higher you bet, the greater your chances of hitting the really big jackpots!",
        "address":"https://static-live.hacksawgaming.com/1025/1.12.0/index.html?language=en&channel=desktop&gameid=1025&mode=2&token=b3225352-5d17-773a-7898-d94e54e87419&lobbyurl=https%3A%2F%2Fwww-dev.multilotto.com%2Fen%2Fscratchcards&currency=EUR&partner=multilotto_stg_mt&env=https://rgs-hacksaw-fun-stg.hacksawgaming.com/api"
      }
		]
	})
}

export const getSearchCasinoList = ()=> {
	return Mock.mock({
		"status":0,
    "code":0,
		"data":[
		  {
        "name":"Temple Tumble",
        "play_url":"templetumble",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd42ed4ab4.png"
      },
      {
        "name":"The Golden Chase",
        "play_url":"thegoldenchase",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd451860a8.png"
      },
      {
        "name":"Splendour Forest",
        "play_url":"splendourforest",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd3de5e748.png"
      },
      {
        "name":"Tower Tumble",
        "play_url":"towertumble",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd49fc0f48.png"
      },
      {
        "name":"Wildchemy",
        "play_url":"wildchemy",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd4fa9c088.png"
      },
      {
        "name":"Trail Blazer",
        "play_url":"trailblazer",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd4b885ad3.png"
      },
      {
        "name":"TNT Tumble",
        "play_url":"tnttumble",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd48525092.png"
      },
      {
        "name":"Hellcatraz",
        "play_url":"hellcatraz",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd5beba98b.png"
      },
      {
        "name":"Yummy Wilds",
        "play_url":"yummywilds",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd5091b951.png"
      },
      {
        "name":"Mega Flip",
        "play_url":"megaflip",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd1ebf30cf.png"
      },
      {
        "name":"Marching Legions",
        "play_url":"marchinglegions",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd1dd82be9.png"
      },
      {
        "name":"Aztek Luck",
        "play_url":"aztekluck",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dce12c639e.png"
      },
      {
        "name":"Nerves of Steal",
        "play_url":"nervesofsteal",
        "provider":"Gluck",
        "logo":""
      },
      {
        "name":"Jingle Up",
        "play_url":"jingleup",
        "provider":"Gluck",
        "logo":""
      },
      {
        "name":"Golden Boot",
        "play_url":"goldenboot",
        "provider":"Gluck",
        "logo":""
      },
      {
        "name":"Diamond Deal",
        "play_url":"diamonddeal",
        "provider":"Gluck",
        "logo":""
      },
      {
        "name":"Christmas Deal",
        "play_url":"christmasdeal",
        "provider":"Gluck",
        "logo":""
      },
      {
        "name":"6 wild sharks",
        "play_url":"6wildsharks",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5fab5822d8f2e.png"
      },
      {
        "name":"Miami Multiplier",
        "play_url":"miamimultiplier",
        "provider":"Hacksaw",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dcc704a1ab.png"
      },
      {
        "name":"Blackjack+",
        "play_url":"blackjackplus",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5ed7013e6ede4.png"
      },
      {
        "name":"Polar Paws",
        "play_url":"polarpaws",
        "provider":"Relax Gaming",
        "logo":""
      },
      {
        "name":"Hall of the Mountain King",
        "play_url":"hallofthemountainking",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5cdbf32fb1296.jpg"
      },
      {
        "name":"Tales of Dr. Dolittle",
        "play_url":"talesofdrdolittle",
        "provider":"Relax Gaming",
        "logo":""
      },
      {
        "name":"Volcano Riches",
        "play_url":"Volcano Riches",
        "provider":"Relax Gaming",
        "logo":""
      },
      {
        "name":"GoldLab",
        "play_url":"goldlab",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/quickspin/thumbnails/goldlab.png"
      },
      {
        "name":"la Fiesta",
        "play_url":"lafiesta",
        "provider":"Relax Gaming",
        "logo":"https://d2ss3jaw6yg7ks.cloudfront.net/assets/img/casino/thumbnails/5f6dd18c46a20.png"
      }
		]
	})
}