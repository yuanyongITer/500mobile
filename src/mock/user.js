import Mock from 'mockjs'

export const login = (option)=> {
	let { userName, password } = JSON.parse(option.body);
	return {
		"status":0,
	  "code":0,
		"data":{
      "username": userName,
      "password": password,
      "nation": 'china',
      "email": "1040685091@qq.com",
      "phone": 13691781061
		}
	}
}

export const logon = (option)=> {
	let { userName, password, email, phone, gender, protocol } = JSON.parse(option.body);
	return {
		"status":0,
	  "code":0,
		"data":{
      "username": userName,
      "password": password,
      "email": email,
      "phone": phone,
      "gender": gender,
      "protocol": protocol
		}
	}
}