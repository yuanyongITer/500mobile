import Vue from 'vue'
import Vuex from 'vuex'
import {login,logon} from '@/api/user'
import {localSave,localRead,localRemove} from '@/lib/tools'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		appLoading: false,
		token: localRead('token'),
		userName: localRead('userName'),
		password: localRead('password'),
		nation: localRead('nation'),
		phone: localRead('phone'),
		email: localRead('email'),
		city: localRead('city'),
		gender: localRead('gender'),
		protocol: localRead('protocol'),
		realMoney: localRead('realMoney'),
		lottoMoney: localRead('lottoMoney'),
		casinoMoney: localRead('casinoMoney'),
		hasGetInfo: localRead('hasGetInfo'),
		isNewUser: localRead('isNewUser'),
		isSubscribe: localRead('isSubscribe')
	},
	mutations: {
		setAppLoading(state,status) {
			state.appLoading = status
		},
		setToken(state, token) {
			state.token = token
			localSave('token',token)
		},
		setUserName(state,userName) {
			state.userName = userName
			localSave('userName',userName)
		},
		setPassword(state, password) {
			state.password = password
			localSave('password',password)
		},
		setEmail(state, email) {
			state.email = email
			localSave('email',email)
		},
		setPhone(state, phone) {
			state.phone = phone
			localSave('phone',phone)
		},
		setCity(state, city) {
			state.city = city
			localSave('city',city)
		},
		setNation(state, nation) {
			state.nation = nation
			localSave('nation',nation)
		},
		setGender(state, gender) {
			state.gender = gender
			localSave('gender',gender)
		},
		setProtocol(state, protocol) {
			state.protocol = protocol
			localSave('protocol',protocol)
		},
		setRealMoney(state, realMoney) {
			state.realMoney = realMoney
			localSave('realMoney',realMoney)
		},
		setLottoMoney(state, lottoMoney) {
			state.lottoMoney = lottoMoney
			localSave('lottoMoney',lottoMoney)
		},
		setCasinoMoney(state, casinoMoney) {
			state.casinoMoney = casinoMoney
			localSave('casinoMoney',casinoMoney)
		},
		setHasGetInfo(state, status) {
			state.hasGetInfo = status
			localSave('hasGetInfo', status)
		},
		setNewUser(state,status) {
			state.isNewUser = status
			localSave('isNewUser',status)
		},
		setSubscribe(state,status) {
			state.isSubscribe = status
			localSave('isSubscribe',status)
		}
	},
	actions: {
		//注册
		handleLogon({commit},{userName,password,phone,email,gender,protocol}) {
			userName = userName.trim()
			return new Promise((resolve,reject)=>{
				//请求接口
				logon({
					userName,
					password,
					phone,
					email,
					gender,
					protocol
				}).then((res)=>{
					const data = res.data;
					commit('setToken', 'token')
					commit('setUserName',data.username)
					commit('setPassword', data.password)
					commit('setEmail', data.email)
					commit('setPhone', data.phone)
					commit('setGender', data.gender)
					commit('setProtocol', data.protocol)
					commit('setCity', 'shenzhen')
					commit('setNation', 'china')
					commit('setNewUser', 0)
					commit('setHasGetInfo', 1)
					commit('setRealMoney', 0)
					commit('setLottoMoney', 0)
					commit('setCasinoMoney', 0)
					resolve()
				}).catch((error)=>{
					reject(error)
				})
			})
		},
		//登录
		handleLogin({commit},{email,password}) {
			email = email.trim();
			return new Promise((resolve,reject)=>{
				//请求接口
				login({
					email,
					password
				}).then((res)=>{
					const data = res.data;
					let realMoney = localRead('realMoney') ? localRead('realMoney'):0;
					let lottoMoney = localRead('lottoMoney') ? localRead('lottoMoney'):0;
					let casinoMoney = localRead('casinoMoney') ? localRead('casinoMoney'):0;
					commit('setToken', 'token')
					commit('setUserName','yuany')
					commit('setEmail',data.email)
					commit('setPassword', data.password)
					commit('setNation', data.nation)
					commit('setHasGetInfo', 1)
					commit('setNewUser', 0)
					commit('setRealMoney',realMoney)
					commit('setLottoMoney',lottoMoney)
					commit('setCasinoMoney',casinoMoney)
					resolve()
				}).catch((error)=>{
					reject(error)
				})
			})
		},
		//登出
		handleLogOut({state,commit}) {
			return new Promise((resolve,reject)=>{
				//登出无请求接口
				commit('setToken', '')
				commit('setUserName','')
				commit('setEmail','')
				commit('setPassword','')
				commit('setNation','')
				commit('setHasGetInfo',0)
				resolve()
			})
		}
	}
})

export default store