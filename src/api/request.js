import axios from 'axios'
import Router from '@/router/index'

// create an axios instance
const service = axios.create({
  baseURL: "", // url = base api url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// request拦截器 request interceptor
service.interceptors.request.use(
  config => {
    // 不传递默认开启loading
    // if (!config.hideloading) {
    //   Toast.loading({
    //     forbidClick: true
    //   })
    // }
    // if (store.getters.token) {
    //   config.headers['X-Token'] = ''
    // }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// respone拦截器
service.interceptors.response.use(
  (response) => {
    const res = response.data;
    return res;
  },(error) => {
    if (error.response) {
      switch (error.response.code) {
        case 401:
          router.replace({
            path:'login',
            query:{ redirect: router.currentRoute.fullPath }//将跳转的路由path作为参数，登录成功后跳转到该路由
          })
      }
    }
    return Promise.reject(error.response)
  }
)

export default service
