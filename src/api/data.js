
import request from './request'

export const getQuickInfo = () => {
	return request({
		url: '/api/getQuickInfo',
		method: 'get'
	})
}

export const getBanList = () => {
	return request({
		url: '/api/getBanList',
		method: 'get'
	})
}

export const getLanList = () => {
	return request({
		url: '/api/getLanList',
		method: 'get'
	})
}

export const getPickList = () => {
	return request({
		url: '/api/getPickList',
		method: 'get'
	})
}

export const getDrawList = () => {
	return request({
		url: '/api/getDrawList',
		method: 'get'
	})
}

export const getGameInfo = () => {
	return request({
		url: '/api/getGameInfo',
		method: 'get'
	})
}

export const getGameMenu = () => {
	return request({
		url: '/api/getGameMenu',
		method: 'get'
	})
}

export const getNewsInfo = () => {
	return request({
		url: '/api/getNewsInfo',
		method: 'get'
	})
}

export const getNewsDetail = (news_id) => {
	return request({
		method: 'post',
		url: '/api/getNewsDetail',
		data: {
			news_id 
		}
	})
}

export const getTicket = (ticket_id) => {
	return request({
		method: 'post',
		url: '/api/getTicket',
		data: {
			ticket_id
		}
	})
}

export const getPlayInfo = () => {
	return request({
		url: '/api/getPlayInfo',
		method: 'get'
	})
}

export const getPlayState = () => {
	return request({
		url: '/api/getPlayState',
		method: 'get'
	})
}

export const getLottoMenu = () => {
	return request({
		url:'/api/getLottoMenu',
		method:'get'
	})
}

export const getLottoInfo = () => {
	return request({
		url: '/api/getLottoInfo',
		method: 'get'
	})
}

export const getResultInfo = () => {
	return request({
		url: '/api/getResultInfo',
		method: 'get'
	})
}

export const getResultState = () => {
	return request({
		url: '/api/getResultState',
		method: 'get'
	})
}

export const getResultDetail = ({name,date}) => {
	return request({
		url: '/api/getResultDetail',
		method: 'post',
		data: {
			name,
			date
		}
	})
}

export const getResultDraw = (name) => {
	return request({
		url: `/api/getResultDraw/${name}`,
		method: 'post'
	})
}

export const getMethodList = () => {
	return request({
		url:'/api/getMethodList',
		method:'get'
	})
}

export const getAccountInfo = (realMoney) => {
	return request({
		url:'/api/getAccountInfo',
		method:'post',
		data: {
			realMoney
		}
	})
}

export const getNoticeList = () => {
	return request({
		url:'/api/getNoticeList',
		method:'get'
	})
}

export const getWinList = () => {
	return request({
		url:'/api/getWinList',
		method:'get'
	})
}

export const getResultList = () => {
	return request({
		url:'/api/getResultList',
		method:'get'
	})
}

export const getOrderList = () => {
	return request({
		url: '/api/createorder',
		method: 'get'
	})
}

export const getPayoutInfo = () => {
	return request({
		url:'/api/getPayoutInfo',
		method: 'get'
	})
}

export const getPayoutList = () => {
	return request({
		url:'/api/getPayoutList',
		method: 'get'
	})
}

export const getPayoutNumber = (params) => {
	return request({
		url:'/api/getPayoutNumber',
		method: 'post',
		data: {
			params
		}
	})
}

export const getWeekList = () => {
	return request({
		url:'/api/getWeekList',
		method:'post'
	})
}

export const getBetsList = () => {
	return request({
		url:'/api/getBetsList',
		method:'post'
	})
}

export const addBetsList = () => {
	return request({
		url:'/api/addBetsList',
		method:'post'
	})
}

export const getCasinoInfo = () => {
	return request({
		url: '/api/getCasinoInfo',
		method: 'get'
	})
}

export const getGameDetailInfo = (casino_id) => {
	return request({
		method: 'post',
		url: '/api/getGameDetailInfo',
		data: {
			casino_id
		}
	})
}

export const getSlideCasinoList = () => {
	return request({
		method: 'post',
		url: '/api/getSlideCasinoList'
	})
}

export const getSearchCasinoList = () => {
	return request({
		method: 'post',
		url: '/api/getSearchCasinoList'
	})
}