
import request from './request'

export const login = ({userName,password}) => {
	return request({
		method: 'post',
		url: '/api/login',
		data: {
			userName,
			password
		}
	})
}

export const logon = ({userName,password,phone,email,gender,protocol}) => {
	return request({
		method: 'post',
		url: '/api/logon',
		data: {
			userName,
			password,
			phone,
			email,
			gender,
			protocol
		}
	})
}